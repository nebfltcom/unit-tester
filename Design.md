## Single Instance Sequence

``` mermaid
sequenceDiagram
  participant S as Server
  participant G as Game Process
  S->>G: Start Game Instance
  G->>S: Requesting Test
  S->>G: Sending Test
  activate G
  G->>S: Sending Version
  Note right of G: Executing Test
  G->>S: Sending Results
  deactivate G
```

## Cluster Sequence

``` mermaid
sequenceDiagram
  participant S as Server
  participant GM as Game Manager
  participant GI as Game Instance
  participant GP as Game Process
  GM->>GI: Create Game Instance
  GI->>GP: Start Game Process
  GM->>S: Requesting Test
  S->>S: Generate Test Ticket
  S-->>GM: Sending Test Info
  GM-->>GI: Store Test Info
  GI->>GI: Setup Test XMLs
  GP->>GM: Requesting Test
  GM-->>GP: Sending Test Info
  activate GP
  GP->>GM: Sending Version Info
  GM-->>GI: Store Version Info
  Note right of GP: Executing Test
  GP->>GM: Sending Event Log
  deactivate GP
  GM->>GI: Grab Test and Version Info
  GI-->>GM: Give Test and Version Info
  GM->>S: Sending Results
```