# NFC Automated Tester

Automated Tester mod for NEBULOUS: Fleet Command weapons and systems to automate data gathering

# Usage

## Prerequisite

1) .NET SDK 6.0.200
    - https://dotnet.microsoft.com/en-us/download/visual-studio-sdks.
2) Node.js v16.13.2 and NPM 8.1.2
    - https://nodejs.org/en/download/

*Note: Exact versions are not strictly required and older versions may work. These version numbers are provided for replicating known working setup.*

## Automatic Installation

1) Git clone or download this repository (https://gitlab.com/nebfltcom/auto-tester/-/archive/main/auto-tester-main.zip). Unzip if needed.
2) Open up a command prompt or powershell session and navigate to the folder that contains this file. The instructions below will refer to this directory as `<repository directory>`.
3) Run the command `npm install`.
4) Run the command `npm run setup`.

## Manual Installation

1) Git clone or download this repository (https://gitlab.com/nebfltcom/auto-tester/-/archive/main/auto-tester-main.zip). Unzip if needed.
2) Open up a command prompt or powershell session and navigate to the folder that contains this file. The instructions below will refer to this directory as `<repository directory>`.
3) Run the command `npm install`.
4) Run the command `npm run server`. This should create the `<repository directory>\tmp` directory (with multiple subdirectories). The command should output "Game not found in tmp/gamedir", this is expected and can be ignored.
5) Copy the game into `<repository directory>\tmp\gamedir`. The instructions below will refer to this directory as `<game directory>`.
6) Download BepInEx 5.4.19 x64 (https://github.com/BepInEx/BepInEx/releases/download/v5.4.19/BepInEx_x64_5.4.19.0.zip).
7) Extract the BepInEx zip contents into `<game directory>`, overwriting files as needed.
8) Run `<game directory>\Nebulous.exe`, verifying `<game directory>\BepInEx\plugins` gets created. Quit the game when this folder is created.
9) *(Optional)* Edit `<game directory>\BepInEx\config\BepInEx.cfg` and replace line 48 with `Enabled = true`.
10) Copy all `.dll` files in `<game directory>\Nebulous_Data\Managed` to `<repository directory>\src\plugin\lib`.
11) In the command prompt or powershell session, navigate to `<repository directory>\src\plugin`.
12) Run the command `dotnet build`.
13) Copy these files from `<repository directory>\src\plugin\bin\Debug\net46` to `<game directory>\BepInEx\plugins`.
    - `Newtonsoft.Json.dll`
    - `autotester.dll`
14) Copy `C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\v3.0\System.Runtime.Serialization.dll` to `<game directory>\BepInEx\plugins`.

### (Optional) Replace steam_api64.dll

Without replacing the steam_api64.dll, running the tester will block you from playing any other game (or run multiple instances of the tester).

1) Download Goldberg Emulator (https://mr_goldberg.gitlab.io/goldberg_emulator/) or compile it yourself (https://gitlab.com/Mr_Goldberg/goldberg_emulator).
2) Replace `<game directory>\Nebulous_Data\Plugins\x86_64\steam_api64.dll` with the Goldberg version.

**Please do not use this to enable piracy! Support the official game at https://store.steampowered.com/app/887570/NEBULOUS_Fleet_Command/**

## Configuration

Before using the automated tester, configuring the application may be needed. By default, only custom tests under `<repository directory>\tmp\custom` are executed. The file `config.json` (which is created during step 4 of the installation process) contains all the configuration options.

``` js
{
  "loadTime": 180, //How long to wait for game to load before restarting the game process
  "cluster": { //Used if multiple computers / VMs are running tests simultaneously
    "currentNumber": 1,
    "totalNumber": 1
  },
  "maxTimeScale": 10, //Used to cap time scale, may need to be adjusted for lower end machines or VMs
  "customTests": true, //If enabled, will run custom tests found under <repository directory>\tmp\custom
  "regularTests": false, //If enabled, will run tests defined in the repository, found in <repository directory>\src\test
  "pdtTests": {
    "rangeTest": false, //If enabled, will run pdt tests defined in the repository, found in <repository directory>\src\test\pdt.js
    "orientationTest": false, //If enabled, will run pdt tests defined in the repository, found in <repository directory>\src\test\pdt.js
    "radarTest": false, //If enabled, will run pdt tests defined in the repository, found in <repository directory>\src\test\pdt.js
    "permutationTest": false //If enabled, will run pdt tests defined in the repository, found in <repository directory>\src\test\pdt.js
  },
  "ttkTests": {
    "stationaryTests": false //If enabled, will run pdt tests defined in the repository, found in <repository directory>\src\test\ttk.js
  }
}
```

For the `cluster` subobject, this is used to skip tests so that only one machine will execute the test.
For example, if two computers are set up and ready:
 - Computer one is configured as `currentNumber: 1` and `totalNumber: 2`, it will only execute test odd numbered tests.
 - Computer two is configured as `currentNumber: 2` and `totalNumber: 2`, it will only execute test even numbered tests.

*Note: This only works if all machines have the exact same tests enabled (and exact same custom tests if there are any)*

## Usage

### Designing Custom Tests

Custom test designer tool is available at https://nebfltcom.gitlab.io/auto-tester/ (or run `npm run web-dev` and go to `https://localhost:1234`). Each field should be self-explanatory. When downloading the test (it should download a `.json` file), move it to `<repository directory>\tmp\custom`.

Available orders may change, so looking at `<repository directory>\src\plugin\Order.cs` is the best way to figure out which order types are available (and what data they use).

### Executing Tests

This process will start executing custom tests.

1) Double check `<repository directory>\config.json` settings are correct.
2) Open up a command prompt or powershell session and navigate to `<repository directory>`.
3) Run the command `npm run server`.
4) Wait for the program to exit, which can take a while depending on the type and number of tests.

### Process Data

This process will start generating charts if defined in `<repository directory>\src\server\charts`.

2) Open up a command prompt or powershell session and navigate to `<repository directory>`.
3) Run the command `npm run chart`.
4) Wait for the program to exit, which can take a while depending on the type and number of charts.
