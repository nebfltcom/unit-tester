
const config = require('../common/config');
const custom = require('./test/custom');
const pdt = require('./test/pdt');
const gun = require('./test/gun');
const validation = require('./test/validation');
const fake = require('./test/fake');

exports.generateTests = () => {
  let tests = [];
  if(config.server.customTests) {
    tests.push(custom.generateTests());
  }
  if(config.server.regularTests) {
    tests.push(pdt.generateTests());
    tests.push(gun.generateTests());
    tests.push(validation.generateTests());
  }
  tests.push(fake.generateTests());
  return tests.flat();
}