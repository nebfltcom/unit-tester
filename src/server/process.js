const fs = require('fs');
const path = require('path');
const moment = require('moment');
const utils = require('../common/utils');

console.info('[Process] Clear data directory');
try { fs.rmSync(path.resolve('./tmp/data'), { recursive: true, force: true }); } catch(err) {}
try { fs.mkdirSync(path.resolve('./tmp/data')); } catch(err) {}

let startTime = Date.now();
let lastUpdate = startTime;
let resultList = fs.readdirSync(path.resolve('./tmp/results/')).filter(e => e.includes('.json'));
let dataset = [];
console.info(`[Process] Combining results into data`);
for(let i = 0;i < resultList.length;i++) {
  if(Date.now() > lastUpdate + 5000) {
    console.info(`[Process] ${i} results out of ${resultList.length} processed`);
    let runTime = Date.now() - startTime;
    let resultsLeft = resultList.length - i;
    let processedPerSecond = i / (runTime / 1000);
    let predictedDuration = resultsLeft / processedPerSecond;
    let predictedFinishDate = moment((predictedDuration * 1000) + Date.now());
    console.info(`[Process] ${(processedPerSecond).toFixed(2)} processed results per second`);
    console.info(`[Process] Predicted to finish ${predictedFinishDate.fromNow()} (${predictedFinishDate.format('dddd, MMMM Do YYYY, h:mm:ss a')})`);
    lastUpdate = Date.now();
  }

  let resultPath = resultList[i];
  let result = JSON.parse(fs.readFileSync(path.resolve(`./tmp/results/${resultPath}`)));
  let dataPath = path.resolve(`./tmp/data/${utils.formatName(result.name).substring(0, 100)}.json`);
  
  let data = {
    name: result.name,
    test: result.test,
    trials: []
  };
  let found = false;
  for(let d of dataset) {
    if(d.name === data.name) {
      data = d;
      found = true;
    }
  }
  if(!found) {
    if(fs.existsSync(dataPath)) {
      data = JSON.parse(fs.readFileSync(dataPath));
    }
    dataset.push(data);
  }
  data.trials.push({
    testNumber: result.testNumber,
    version: result.version,
    eventLog: result.eventLog
  });
}

for(let data of dataset) {
  let dataPath = path.resolve(`./tmp/data/${utils.formatName(data.name).substring(0, 100)}.json`);
  console.info(`[Process] Writing data to ${dataPath}`);
  fs.writeFileSync(dataPath, `{"name": "${data.name}","test": ${JSON.stringify(data.test)},"trials": [`);
  data.trials.forEach((e, i) => {
    fs.writeFileSync(dataPath, JSON.stringify(e) + (i < data.trials.length - 1 ? ',\n' : ''), { flag: 'a' });
  });
  fs.writeFileSync(dataPath, `]}`, { flag: 'a' });
}