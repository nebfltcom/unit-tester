module.exports = {
  name: 'Base Test',
  type: 'Base',
  testAmount: 1, //How many times should this test be run
  maxTime: 1800, //Max amount of time to run the test in seconds
  timeScale: 10, //In-game time scale to use
  description: 'Example test with no real purpose outside of showing what tests look like',
  cameraFollow: true, //Used to have the camera follow the target (pointing at the attacker)
  custom: {}, //Used by chart scripts for custom properties / sorting
  setup: {
    attackingShip: {
      ship: { //Used to generate fleet xml
        hullType: 'sprinter',
        mounts: [],
        compartments: [],
        modules: []
      },
      position: {
        x: 5000,
        y: 0,
        z: 0,
        isStatic: true //Used to lock ship in a position
      },
      rotation: {
        rx: 0,
        ry: 0,
        rz: 0,
        isStatic: true //Used to lock ship in an orientation
      },
      velocity: {
        vx: 0,
        vy: 0,
        vz: 0,
        isStatic: true //Used to lock ship in a trajectory
      },
      circleMove: { //Used to manual move the ship in a circle
        enable: false,
        targetRange: 0,
        targetVelocity: 0
      },
      orders: []
    },
    targetShip: {
      ship: { //Used to generate fleet xml
        hullType: 'sprinter',
        mounts: [],
        compartments: [],
        modules: []
      },
      position: {
        x: 0,
        y: 0,
        z: 0,
        isStatic: true //Used to lock ship in a position
      },
      rotation: {
        rx: 0,
        ry: 0,
        rz: 0,
        isStatic: true //Used to lock ship in an orientation
      },
      velocity: {
        vx: 0,
        vy: 0,
        vz: 0,
        isStatic: true //Used to lock ship in a trajectory
      },
      circleMove: { //Used to manual move the ship in a circle
        enable: false,
        targetRange: 0,
        targetVelocity: 0
      },
      orders: []
    }
  }
};
