const config = require('../../common/config');
const pdt = require('./validation/pdt');

exports.generateTests = () => {
  let tests = [];
  if(config.server.validationTests.pdtTests) {
    tests.push(pdt.generateTests());
  }
  return tests.flat();
}