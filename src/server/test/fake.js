const deepcopy = require('deepcopy');
const config = require('../../common/config');
const baseTest = require('./base');

exports.generateTests = () => {
  let tests = [];
  if(config.server.fakeTests) {
    for(let i = 0;i < 10;i++) {
      let currTest = deepcopy(baseTest);
      currTest.name = `Fake Test ${i + 1}`;
      currTest.type = `Fake Test`;
      currTest.custom = {
        index: i
      };
      currTest.testAmount = 10;
      currTest.maxTime = 10;
      currTest.timeScale = 1;
      currTest.description = `Fake tests used for debugging monitoring tools.`;
      tests.push(currTest);
    }
  }
  return tests;
}