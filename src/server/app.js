const express = require('express');
const fs = require('fs');
const path = require('path');
const config = require('../common/config');
const utils = require('../common/utils');
const test = require('./test');
const validate = require('./validate');

const app = express();
const port = 9998;

//Creating data directories
try { fs.mkdirSync(path.resolve('./tmp')); } catch(err) {}
try { fs.mkdirSync(path.resolve('./tmp/results')); } catch(err) {}
try { fs.mkdirSync(path.resolve('./tmp/charts')); } catch(err) {}
try { fs.mkdirSync(path.resolve('./tmp/customtests')); } catch(err) {}
try { fs.mkdirSync(path.resolve('./tmp/data')); } catch(err) {}

//Generating all tests
let tests = test.generateTests();

//Grabbing previous results
let previousResults = [];
let previousResultList = fs.readdirSync(path.resolve('./tmp/results/')).filter(e => e.includes('.json'));
for(let result of previousResultList) {
  let currPath = path.resolve(`./tmp/results/${result}`);
  let fileResult = JSON.parse(fs.readFileSync(currPath));
  if(validate.validateResult(fileResult)) {
    previousResults.push({
      name: fileResult.name,
      testNumber: fileResult.testNumber
    });
  }
  else {
    console.info(`[Server] Result at ${currPath} is invalid`);
    try { fs.rmSync(currPath); } catch(err) {}
  }
}

//Generating test tickets still needed
let testTicketsQueued = [];
for(let i = 0;i < tests.length;i++) {
  let currTest = tests[i];
  for(let j = 0;j < currTest.testAmount;j++) {
    let needed = true;
    for(let result of previousResults) {
      if(result.name === currTest.name && result.testNumber === j + 1) {
        needed = false;
      }
    }
    if(needed) {
      testTicketsQueued.push({
        index: i,
        testNumber: j + 1,
        startDate: 0
      });
    }
  }
}

let testTicketsInProgress = [];
//Check if test tickets are stale
const refreshStaleTestTickets = () => {
  let currDate = Date.now();
  for(let i = 0;i < testTicketsInProgress.length;i++) {
    let currTestTicket = testTicketsInProgress[i];
    let currTest = tests[currTestTicket.index];
    let deadline = currTestTicket.startDate + (utils.maxTime(currTest, config.common.loadTime) * 2);
    if(deadline < currDate) {
      testTicketsQueued.splice(0, 0, currTestTicket);
      testTicketsInProgress.splice(i, 1);
      i--;
    }
  }
}
//Refresh any stale tickets every minute
setInterval(refreshStaleTestTickets.bind(this), 60 * 1000);

const getNextTestTicket = () => {
  refreshStaleTestTickets();
  if(testTicketsQueued.length > 0) {
    let currTestTicket = testTicketsQueued[0];
    currTestTicket.startDate = Date.now();
    testTicketsInProgress.push(currTestTicket);
    testTicketsQueued.splice(0, 1);
    return currTestTicket;
  }
  else if(testTicketsInProgress.length > 0) {
    return null;
  }
  else {
    console.info(`[Server] No more tests, exiting...`);
    process.exit();
  }
}

let testTicketsDone = [];
const removeTestTicket = (currResult) => {
  //Remove test ticket from in progress list
  for(let i = 0;i < testTicketsInProgress.length;i++) {
    let currTestTicket = testTicketsInProgress[i];
    let currTest = tests[currTestTicket.index];
    if(currTest.name === currResult.name && currTestTicket.testNumber === currResult.testNumber) {
      currTestTicket.endDate = Date.now();
      testTicketsDone.push(currTestTicket);
      testTicketsInProgress.splice(i, 1);
      i--;
    }
  }
  //Remove test ticket from queue
  for(let i = 0;i < testTicketsQueued.length;i++) {
    let currTestTicket = testTicketsQueued[i];
    let currTest = tests[currTestTicket.index];
    if(currTest.name === currResult.name && currTestTicket.testNumber === currResult.testNumber) {
      currTestTicket.endDate = Date.now();
      testTicketsDone.push(currTestTicket);
      testTicketsQueued.splice(i, 1);
      i--;
    }
  }
}

app.use(express.json({ limit: '1tb' }));
app.use(express.static(path.join(__dirname, '/monitor')));
app.get('/test', (req, res) => {
  console.info(`[Server] Sending test info`);
  let currTestTicket = getNextTestTicket();
  if(currTestTicket === null) {
    res.send({});
  }
  else {
    res.send({
      test: tests[currTestTicket.index],
      testNumber: currTestTicket.testNumber
    });
  }
});
app.get('/monitor', (req, res) => {
  res.send({
    testTicketsQueued: testTicketsQueued,
    testTicketsInProgress: testTicketsInProgress,
    testTicketsDone: testTicketsDone
  });
});
app.put('/result', (req, res) => {
  let data = req.body;
  console.info(`[Server] Received test result`);
  let currResult = data;
  let alreadyDone = false;
  //Remove test ticket from in progress list
  for(let i = 0;i < testTicketsDone.length;i++) {
    let currTestTicket = testTicketsDone[i];
    let currTest = tests[currTestTicket.index];
    if(currTest.name === currResult.name && currTestTicket.testNumber === currResult.testNumber) {
      alreadyDone = true;
    }
  }
  if(!alreadyDone && validate.validateResult(currResult)) {
    let fileName = `${utils.formatName(currResult.name).substring(0, 100)}_${currResult.testNumber}_${Date.now()}.json`;
    fs.writeFileSync(path.resolve('./tmp/results/' + fileName), JSON.stringify(currResult, null, 2));
    removeTestTicket(currResult);
  }
  res.end();
});
app.put('/remove-result', (req, res) => {
  let data = req.body;
  console.info(`[Server] Removing test ticket`);
  let currResult = data;
  if(config.server.canRemoveTicket) {
    removeTestTicket(currResult);
  }
  res.end();
});
app.listen(port, () => {
  console.info(`[Server] NEBFLTCOM Automated Testing Server is listening on port ${port}`);
  console.info(`[Server] Monitoring is available at http://localhost:${port}`);
});