//Enable debugging when debug function is called in console
window.debug = () => {
  document.getElementById('debugContent').style.visibility = 'visible';
}

let testsQueued = [];
//Buttons used for debugging monitoring
document.getElementById('startTestDebugButton').onclick = async () => {
  let response = await fetch(`${window.location.origin}/test`);
  let data = await response.json();
  testsQueued.push(data);
}

document.getElementById('finishTestDebugButton').onclick = async () => {
  if(testsQueued.length > 0) {
    await fetch(`${window.location.origin}/remove-result`, {
      method: 'put',
      body: JSON.stringify({
        name: testsQueued[0].test.name,
        testNumber: testsQueued[0].testNumber
      }),
      headers: {'Content-Type': 'application/json'}
    });
    testsQueued.splice(0,1);
  }
}