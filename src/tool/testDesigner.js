const dict = require('../common/dict');
const downloadManager = require('./testDesigner/downloadManager');

const baseOrderData = `{
  "orderStringData1": "",
  "orderStringData2": "",
  "orderStringData3": "",
  "orderStringData4": "",
  "orderStringData5": "",
  "orderIntData1": 0,
  "orderIntData2": 0,
  "orderIntData3": 0,
  "orderIntData4": 0,
  "orderIntData5": 0,
  "orderFloatData1": 0,
  "orderFloatData2": 0,
  "orderFloatData3": 0,
  "orderFloatData4": 0,
  "orderFloatData5": 0,
  "orderBoolData1": false,
  "orderBoolData2": false,
  "orderBoolData3": false,
  "orderBoolData4": false,
  "orderBoolData5": false,
  "orderVector3Intdata1": {
    "x": 0,
    "y": 0,
    "z": 0
  },
  "orderVector3Intdata2": {
    "x": 0,
    "y": 0,
    "z": 0
  },
  "orderVector3Intdata3": {
    "x": 0,
    "y": 0,
    "z": 0
  },
  "orderVector3Intdata4": {
    "x": 0,
    "y": 0,
    "z": 0
  },
  "orderVector3Intdata5": {
    "x": 0,
    "y": 0,
    "z": 0
  },
  "orderVector3data1": {
    "x": 0,
    "y": 0,
    "z": 0
  },
  "orderVector3data2": {
    "x": 0,
    "y": 0,
    "z": 0
  },
  "orderVector3data3": {
    "x": 0,
    "y": 0,
    "z": 0
  },
  "orderVector3data4": {
    "x": 0,
    "y": 0,
    "z": 0
  },
  "orderVector3data5": {
    "x": 0,
    "y": 0,
    "z": 0
  }
}`;

const perOrderHtml = (shipName, amount) => {
  return `<div id="testDesigner-${shipName.toLowerCase()}Order${amount}" class="mb-2 col row">
  <div class="col">
    <label for="testDesigner-${shipName.toLowerCase()}Order${amount}TypeInput" class="form-label">${shipName} Order ${amount + 1} Type</label>
    <select id="testDesigner-${shipName.toLowerCase()}Order${amount}TypeInput" class="form-select">
      <option value="TransformPosition" selected>Transform Position</option>
      <option value="TransformRotation">Transform Rotation</option>
      <option value="TransformVelocity">Transform Velocity</option>
      <option value="WCON">WCON</option>
      <option value="EMCON">EMCON</option>
      <option value="PDMode">PDMode</option>
      <option value="BattleShort">BattleShort</option>
      <option value="MovementStyle">MovementStyle</option>
      <option value="CameraPosition">CameraPosition</option>
    </select>
  </div>
  <div class="col">
    <label for="testDesigner-${shipName.toLowerCase()}Order${amount}TimeInput" class="form-label">${shipName} Order ${amount + 1} Time (secs)</label>
    <input type="number" value="0" class="form-control" id="testDesigner-${shipName.toLowerCase()}Order${amount}TimeInput" aria-describedby="testDesigner-${shipName.toLowerCase()}Order${amount}TimeDescription">
    <div id="testDesigner-${shipName.toLowerCase()}Order${amount}TimeDescription" class="form-text">Amount of time from start of test to execute order.</div>
  </div>
  <div class="col-6">
    <label for="testDesigner-${shipName.toLowerCase()}Order${amount}DataInput" class="form-label">${shipName} Order ${amount + 1} JSON Data</label>
    <div class="border">
      <textarea rows="5" class="form-control" id="testDesigner-${shipName.toLowerCase()}Order${amount}DataInput" aria-describedby="testDesigner-${shipName.toLowerCase()}Order${amount}DataDescription"></textarea>
    </div>
    <div id="testDesigner-${shipName.toLowerCase()}Order${amount}DataDescription" class="form-text">Text parsed into JSON order data.</div>
  </div>
</div>`;
}

const perComponentHtml = (shipName, componentType, amount) => {
  return `<div id="testDesigner-${shipName.toLowerCase()}${componentType}${amount}" class="mb-2 col row">
  <div class="col-8">
    <label for="testDesigner-${shipName.toLowerCase()}${componentType}${amount}TypeInput" class="form-label">${shipName} ${componentType} ${amount + 1}</label>
    <select id="testDesigner-${shipName.toLowerCase()}${componentType}${amount}TypeInput" class="form-select">
      ${dict.componentNames[componentType.toLowerCase() + 's'].map(e => '<option value="' + e +'">' + e + '</option>\n').reduce((a, c) => a + c)}
      <option value="None" selected>Empty</option>
    </select>
  </div>
  <div class="col">
    <div id="testDesigner-${shipName.toLowerCase()}${componentType}${amount}Munitions"></div>
    <div class="btn-group" role="group">
      <button id="testDesigner-${shipName.toLowerCase()}${componentType}${amount}AddMunitionButton" onclick="window.AddMunition('${shipName}', '${componentType}', ${amount})" type="button" class="btn btn-outline-primary">Add Munition</button>
      <button id="testDesigner-${shipName.toLowerCase()}${componentType}${amount}DeleteMunitionButton" onclick="window.DeleteMunition('${shipName}', '${componentType}', ${amount})" type="button" class="btn btn-outline-danger">Delete Munition</button>
    </div>
  </div>
</div>`;
}

const perMunitionHtml = (shipName, componentType, amount, amount2) => {
  return `<div id="testDesigner-${shipName.toLowerCase()}${componentType}${amount}Munition${amount2}" class="mb-2 col row">
  <div class="col">
    <label for="testDesigner-${shipName.toLowerCase()}${componentType}${amount}Munition${amount2}TypeInput" class="form-label">Munition ${amount2 + 1}</label>
    <select id="testDesigner-${shipName.toLowerCase()}${componentType}${amount}Munition${amount2}TypeInput" class="form-select">
      ${dict.munitionNames.map(e => '<option value="' + e +'">' + e + '</option>\n').reduce((a, c) => a + c)}
    </select>
  </div>
  <div class="col">
    <label for="testDesigner-${shipName.toLowerCase()}${componentType}${amount}Munition${amount2}QuantityInput" class="form-label">Quantity</label>
    <input type="number" value="1" class="form-control" id="testDesigner-${shipName.toLowerCase()}${componentType}${amount}Munition${amount2}QuantityInput">
  </div>
</div>`;
}

const perShipHtml = (shipName) => {
  return `<div class="mb-2">
  <label for="testDesigner-${shipName.toLowerCase()}HullTypeInput" class="form-label">${shipName} Hull Type</label>
  <select id="testDesigner-${shipName.toLowerCase()}HullTypeInput" class="form-select">
    <option value="sprinter" selected>Sprinter Corvette</option>
    <option value="raines">Raines Frigate</option>
    <option value="keystone">Keystone Destroyer</option>
    <option value="vauxhall">Vauxhall Light Cruiser</option>
    <option value="axford">Axford Heavy Cruiser</option>
    <option value="solomon">Solomon Battleship</option>
  </select>
</div>
<div id="testDesigner-${shipName.toLowerCase()}Mounts" class="mb-2 row row-cols-1"></div>
<div class="mb-2">
  <div class="btn-group" role="group">
    <button id="testDesigner-${shipName.toLowerCase()}AddMountButton" type="button" class="btn btn-outline-primary">Add Mount</button>
    <button id="testDesigner-${shipName.toLowerCase()}DeleteMountButton" type="button" class="btn btn-outline-danger">Delete Mount</button>
  </div>
</div>
<div id="testDesigner-${shipName.toLowerCase()}Compartments" class="mb-2 row row-cols-1"></div>
<div class="mb-2">
  <div class="btn-group" role="group">
    <button id="testDesigner-${shipName.toLowerCase()}AddCompartmentButton" type="button" class="btn btn-outline-primary">Add Compartment</button>
    <button id="testDesigner-${shipName.toLowerCase()}DeleteCompartmentButton" type="button" class="btn btn-outline-danger">Delete Compartment</button>
  </div>
</div>
<div id="testDesigner-${shipName.toLowerCase()}Modules" class="mb-2 row row-cols-1"></div>
<div class="mb-2">
  <div class="btn-group" role="group">
    <button id="testDesigner-${shipName.toLowerCase()}AddModuleButton" type="button" class="btn btn-outline-primary">Add Module</button>
    <button id="testDesigner-${shipName.toLowerCase()}DeleteModuleButton" type="button" class="btn btn-outline-danger">Delete Module</button>
  </div>
</div>
<div id="testDesigner-${shipName.toLowerCase()}Position" class="mb-2 row row-cols-4">
  <div class="col">
    <label for="testDesigner-${shipName.toLowerCase()}PositionXInput" class="form-label">Position X (m)</label>
    <input type="number" value="0" class="form-control" id="testDesigner-${shipName.toLowerCase()}PositionXInput">
  </div>
  <div class="col">
    <label for="testDesigner-${shipName.toLowerCase()}PositionYInput" class="form-label">Position Y (m)</label>
    <input type="number" value="0" class="form-control" id="testDesigner-${shipName.toLowerCase()}PositionYInput">
  </div>
  <div class="col">
    <label for="testDesigner-${shipName.toLowerCase()}PositionZInput" class="form-label">Position Z (m)</label>
    <input type="number" value="0" class="form-control" id="testDesigner-${shipName.toLowerCase()}PositionZInput">
  </div>
  <div class="col">
    <div class="form-check">
      <input type="checkbox" value="" checked class="form-check-input" id="testDesigner-${shipName.toLowerCase()}PositionStaticInput" aria-describedby="testDesigner-${shipName.toLowerCase()}PositionStaticDescription">
      <label for="testDesigner-${shipName.toLowerCase()}PositionStaticInput" class="form-check-label">Lock Position</label>
      <div id="testDesigner-${shipName.toLowerCase()}PositionStaticDescription" class="form-text">Used to lock ship in a position.</div>
    </div>
  </div>
</div>
<div id="testDesigner-${shipName.toLowerCase()}Rotation" class="mb-2 row row-cols-4">
  <div class="col">
    <label for="testDesigner-${shipName.toLowerCase()}RotationXInput" class="form-label">Rotation X (deg)</label>
    <input type="number" value="0" class="form-control" id="testDesigner-${shipName.toLowerCase()}RotationXInput">
  </div>
  <div class="col">
    <label for="testDesigner-${shipName.toLowerCase()}RotationYInput" class="form-label">Rotation Y (deg)</label>
    <input type="number" value="0" class="form-control" id="testDesigner-${shipName.toLowerCase()}RotationYInput">
  </div>
  <div class="col">
    <label for="testDesigner-${shipName.toLowerCase()}RotationZInput" class="form-label">Rotation Z (deg)</label>
    <input type="number" value="0" class="form-control" id="testDesigner-${shipName.toLowerCase()}RotationZInput">
  </div>
  <div class="col">
    <div class="form-check">
      <input type="checkbox" value="" checked class="form-check-input" id="testDesigner-${shipName.toLowerCase()}RotationStaticInput" aria-describedby="testDesigner-${shipName.toLowerCase()}RotationStaticDescription">
      <label for="testDesigner-${shipName.toLowerCase()}RotationStaticInput" class="form-check-label">Lock Rotation</label>
      <div id="testDesigner-${shipName.toLowerCase()}RotationStaticDescription" class="form-text">Used to lock ship in an orientation.</div>
    </div>
  </div>
</div>
<div id="testDesigner-${shipName.toLowerCase()}Velocity" class="mb-2 row row-cols-4">
  <div class="col">
    <label for="testDesigner-${shipName.toLowerCase()}VelocityXInput" class="form-label">Velocity X (m/s)</label>
    <input type="number" value="0" class="form-control" id="testDesigner-${shipName.toLowerCase()}VelocityXInput">
  </div>
  <div class="col">
    <label for="testDesigner-${shipName.toLowerCase()}VelocityYInput" class="form-label">Velocity Y (m/s)</label>
    <input type="number" value="0" class="form-control" id="testDesigner-${shipName.toLowerCase()}VelocityYInput">
  </div>
  <div class="col">
    <label for="testDesigner-${shipName.toLowerCase()}VelocityZInput" class="form-label">Velocity Z (m/s)</label>
    <input type="number" value="0" class="form-control" id="testDesigner-${shipName.toLowerCase()}VelocityZInput">
  </div>
  <div class="col">
    <div class="form-check">
      <input type="checkbox" value="" checked class="form-check-input" id="testDesigner-${shipName.toLowerCase()}VelocityStaticInput" aria-describedby="testDesigner-${shipName.toLowerCase()}VelocityStaticDescription">
      <label for="testDesigner-${shipName.toLowerCase()}VelocityStaticInput" class="form-check-label">Lock Velocity</label>
      <div id="testDesigner-${shipName.toLowerCase()}VelocityStaticDescription" class="form-text">Used to lock ship in a trajectory.</div>
    </div>
  </div>
</div>
<div id="testDesigner-${shipName.toLowerCase()}Orders" class="mb-2 row row-cols-1"></div>
<div class="mb-2">
  <div class="btn-group" role="group">
    <button id="testDesigner-${shipName.toLowerCase()}AddOrderButton" type="button" class="btn btn-outline-primary">Add Order</button>
    <button id="testDesigner-${shipName.toLowerCase()}DeleteOrderButton" type="button" class="btn btn-outline-danger">Delete Order</button>
  </div>
</div>`;
}

let rootElement = document.getElementById('testContent');
rootElement.innerHTML += `<div>
  <form>
    <h3>Custom Test</h3>
    <div class="mb-2">
      <label for="testDesigner-testNameInput" class="form-label">Name</label>
      <input type="text" class="form-control" id="testDesigner-testNameInput" aria-describedby="testDesigner-testNameDescription">
      <div id="testDesigner-testNameDescription" class="form-text">Text used to uniquely identify test, which should be short as possible.</div>
    </div>
    <div class="mb-2">
      <label for="testDesigner-testTypeInput" class="form-label">Type</label>
      <input type="text" class="form-control" id="testDesigner-testTypeInput" aria-describedby="testDesigner-testTypeDescription">
      <div id="testDesigner-testTypeDescription" class="form-text">Text used to group similar tests together.</div>
    </div>
    <div class="mb-2">
      <label for="testDesigner-testAmountInput" class="form-label">Amount of Tests</label>
      <input type="number" value="1" class="form-control" id="testDesigner-testAmountInput" aria-describedby="testDesigner-testAmountDescription">
      <div id="testDesigner-testAmountDescription" class="form-text">Number used to dictate how many times this test should be run.</div>
    </div>
    <div class="mb-2">
      <label for="testDesigner-testMaxTimeInput" class="form-label">Max Time (secs)</label>
      <input type="number" value="1800" class="form-control" id="testDesigner-testMaxTimeInput" aria-describedby="testDesigner-testMaxTimeDescription">
      <div id="testDesigner-testMaxTimeDescription" class="form-text">Max amount of time allowed to run this test.</div>
    </div>
    <div class="mb-2">
      <label for="testDesigner-testTimeScaleInput" class="form-label">Time Scale (multiplier)</label>
      <input type="number" value="10" class="form-control" id="testDesigner-testTimeScaleInput" aria-describedby="testDesigner-testTimeScaleDescription">
      <div id="testDesigner-testTimeScaleDescription" class="form-text">In-game time scale to use.</div>
    </div>
    <div class="mb-2">
      <label for="testDesigner-testDescriptionInput" class="form-label">Description</label>
      <textarea rows="3" class="form-control" id="testDesigner-testDescriptionInput" aria-describedby="testDesigner-testDescriptionDescription"></textarea>
      <div id="testDesigner-testDescriptionDescription" class="form-text">Text used to extensively describe the test.</div>
    </div>
    <h3>Attacking Ship</h3>
    ${perShipHtml('Attacker')}
    <h3>Target Ship</h3>
    ${perShipHtml('Target')}
    <button id="testDesigner-testDownloadButton" type="button" class="btn btn-primary">Download</button>
  </form>
</div>`;

['Attacker', 'Target'].forEach(shipName => {
  ['Mount', 'Compartment', 'Module', 'Order'].forEach(componentType => {
    document.getElementById(`testDesigner-${shipName.toLowerCase()}Add${componentType}Button`).onclick = () => {
      let currFound = 0;
      let found = document.getElementById(`testDesigner-${shipName.toLowerCase()}${componentType}${currFound}`) !== null;
      while(found) {
        currFound++;
        found = document.getElementById(`testDesigner-${shipName.toLowerCase()}${componentType}${currFound}`) !== null;
      }
      if(componentType !== 'Order') {
        let componentDiv = document.createElement('div');
        componentDiv.innerHTML = perComponentHtml(shipName, componentType, currFound);
        document.getElementById(`testDesigner-${shipName.toLowerCase()}${componentType}s`).appendChild(componentDiv);
      }
      else {
        let orderDiv = document.createElement('div');
        orderDiv.innerHTML = perOrderHtml(shipName, currFound);
        document.getElementById(`testDesigner-${shipName.toLowerCase()}Orders`).appendChild(orderDiv); 
        document.getElementById(`testDesigner-${shipName.toLowerCase()}Order${currFound}DataInput`).defaultValue = baseOrderData
      }
    };
    document.getElementById(`testDesigner-${shipName.toLowerCase()}Delete${componentType}Button`).onclick = () => {
      let currFound = 0;
      let found = document.getElementById(`testDesigner-${shipName.toLowerCase()}${componentType}${currFound}`) !== null;
      while(found) {
        currFound++;
        found = document.getElementById(`testDesigner-${shipName.toLowerCase()}${componentType}${currFound}`) !== null;
      }
      document.getElementById(`testDesigner-${shipName.toLowerCase()}${componentType}${currFound - 1}`).remove();
    };
  });
});

window.AddMunition = (shipName, componentType, amount) => {
  let currFound = 0;
  let found = document.getElementById(`testDesigner-${shipName.toLowerCase()}${componentType}${amount}Munition${currFound}`) !== null;
  while(found) {
    currFound++;
    found = document.getElementById(`testDesigner-${shipName.toLowerCase()}${componentType}${amount}Munition${currFound}`) !== null;
  }
  let munitionDiv = document.createElement('div');
  munitionDiv.innerHTML = perMunitionHtml(shipName, componentType, amount, currFound);
  document.getElementById(`testDesigner-${shipName.toLowerCase()}${componentType}${amount}Munitions`).appendChild(munitionDiv);
}

window.DeleteMunition = (shipName, componentType, amount) => {
  let currFound = 0;
  let found = document.getElementById(`testDesigner-${shipName.toLowerCase()}${componentType}${amount}Munition${currFound}`) !== null;
  while(found) {
    currFound++;
    found = document.getElementById(`testDesigner-${shipName.toLowerCase()}${componentType}${amount}Munition${currFound}`) !== null;
  }
  document.getElementById(`testDesigner-${shipName.toLowerCase()}${componentType}${amount}Munition${currFound - 1}`).remove();
}

document.getElementById('testDesigner-testDownloadButton').onclick = downloadManager.download;