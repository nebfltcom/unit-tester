const deepcopy = require('deepcopy');
const fileDownload = require('js-file-download');
const baseTest = require('../../server/test/base');
const utils = require('../../common/utils');

const extractShipData = (shipData, shipName) => {
  let res = deepcopy(shipData);
  res.ship.hullType = document.getElementById(`${shipName.toLowerCase()}HullTypeInput`).value;
  ['Mount', 'Compartment', 'Module'].forEach(componentType => {
    let currFound = 0;
    let found = document.getElementById(`${shipName.toLowerCase()}${componentType}${currFound}`) !== null;
    while(found) {
      let currComponent = {
        componentName: document.getElementById(`${shipName.toLowerCase()}${componentType}${currFound}TypeInput`).value
      };
      let currMunitionFound = 0;
      let foundMunition = document.getElementById(`${shipName.toLowerCase()}${componentType}${currFound}Munition${currMunitionFound}`) !== null;
      while(foundMunition) {
        if(!Array.isArray(currComponent.componentData)) {
          currComponent.componentData = [];
        }
        currComponent.componentData.push({
          munitionName: document.getElementById(`${shipName.toLowerCase()}${componentType}${currFound}Munition${currMunitionFound}TypeInput`).value,
          quantity: Number(document.getElementById(`${shipName.toLowerCase()}${componentType}${currFound}Munition${currMunitionFound}QuantityInput`).value)
        });
        currMunitionFound++;
        foundMunition = document.getElementById(`${shipName.toLowerCase()}${componentType}${currFound}Munition${currMunitionFound}`) !== null;
      }
      res.ship[componentType.toLowerCase() + 's'].push(currComponent);
      currFound++;
      found = document.getElementById(`${shipName.toLowerCase()}${componentType}${currFound}`) !== null;
    }
  });
  res.position.x = Number(document.getElementById(`${shipName.toLowerCase()}PositionXInput`).value);
  res.position.y = Number(document.getElementById(`${shipName.toLowerCase()}PositionYInput`).value);
  res.position.z = Number(document.getElementById(`${shipName.toLowerCase()}PositionZInput`).value);
  res.position.isStatic = document.getElementById(`${shipName.toLowerCase()}PositionStaticInput`).checked;
  res.rotation.rx = Number(document.getElementById(`${shipName.toLowerCase()}RotationXInput`).value);
  res.rotation.ry = Number(document.getElementById(`${shipName.toLowerCase()}RotationYInput`).value);
  res.rotation.rz = Number(document.getElementById(`${shipName.toLowerCase()}RotationZInput`).value);
  res.rotation.isStatic = document.getElementById(`${shipName.toLowerCase()}RotationStaticInput`).checked;
  res.velocity.vx = Number(document.getElementById(`${shipName.toLowerCase()}VelocityXInput`).value);
  res.velocity.vy = Number(document.getElementById(`${shipName.toLowerCase()}VelocityYInput`).value);
  res.velocity.vz = Number(document.getElementById(`${shipName.toLowerCase()}VelocityZInput`).value);
  res.velocity.isStatic = document.getElementById(`${shipName.toLowerCase()}VelocityStaticInput`).checked;
  let currFound = 0;
  let found = document.getElementById(`${shipName.toLowerCase()}Order${currFound}`) !== null;
  while(found) {
    let currOrder = {
      type: document.getElementById(`${shipName.toLowerCase()}Order${currFound}TypeInput`).value,
      time: Number(document.getElementById(`${shipName.toLowerCase()}Order${currFound}TimeInput`).value)
    };
    Object.assign(currOrder, JSON.parse(document.getElementById(`${shipName.toLowerCase()}Order${currFound}DataInput`).value));
    res.orders.push(currOrder);
    currFound++;
    found = document.getElementById(`${shipName.toLowerCase()}Order${currFound}`) !== null;
  }
  return res;
}

exports.download = () => {
  let res = deepcopy(baseTest);
  res.name = document.getElementById('testNameInput').value;
  res.type = document.getElementById('testTypeInput').value;
  res.testAmount = Number(document.getElementById('testAmountInput').value);
  res.maxTime = Number(document.getElementById('testMaxTimeInput').value);
  res.timeScale = Number(document.getElementById('testTimeScaleInput').value);
  res.description = document.getElementById('testDescriptionInput').value;
  res.setup.attackingShip = extractShipData(res.setup.attackingShip, 'Attacker');
  res.setup.targetShip = extractShipData(res.setup.targetShip, 'Target');
  fileDownload(JSON.stringify(res, null, 2), `${utils.formatName(res.name).substring(0, 100)}_${Date.now()}.json`);
}