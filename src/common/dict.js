exports.hullTypeName = {
  sprinter: 'Stock/Sprinter Corvette',
  raines: 'Stock/Raines Frigate',
  keystone: 'Stock/Keystone Destroyer',
  vauxhall: 'Stock/Vauxhall Light Cruiser',
  axford: 'Stock/Axford Heavy Cruiser',
  solomon: 'Stock/Solomon Battleship'
};

exports.hullTypeSocketKeys = {
  sprinter: {
    mounts: [
      'wDsRnL5nKkyYvKgD6VcPHg',
      'Z48ot_dQfkWb6AVYjaM_gA',
      'IUdNSVZm2Eu9n3F5HnSAng',
      'ZxY9ONYz80SiLNSObvjNzQ'
    ],
    compartments: [
      'XPaYCjBqdEqBxEIsVLP0oA',
      'xRYIkvssd0mPY3VIgrnQ5A',
      'rXO8xwG1MkqzI_2pU-O_qQ',
      'GefqwCQzg0qXA3EFRmDtPw'
    ],
    modules: [
      '4WpJyiOKVEqCwR99l47MkA',
      '4EHx4mQhNUi-WuuQtQF5fA',
      'TSPPW9ECe06-MGzR1i0WvQ',
      'MBOvGazj6UWpt2OOy44s7w'
    ]
  },
  raines: {
    mounts: [
      'PDKmGfvpykODc3XHDQ7WBw',
      'WwMGqYiU7E6lp7ID47phqA',
      'gkfIwYzn7kGhG6MW9uxBmw',
      'D3zoB0PetEC973iUck_mNQ',
    ],
    compartments: [
      '98N-YI_1WUOs--qPYlt-7g',
      '6Tzo7268MEqgyFxnGvLKUg',
      'O_Y-AJc7r0alOg36rvY6MQ',
      '5APOD4UfSkGdkm9WWdAvLA',
      'lorATFhkTkeZQYThYOHazg'
    ],
    modules: [
      'bRgUlaoQJ0S7M91zMpdAdA',
      'Jmtnwo0KQki5QyEPPlBHrA',
      'V42tXibIR0e4u6riIHYZOw',
      'p-m7ijS6ukuqzuBT5NE_lA',
      'foApM84hT0GpEw4GaFv38w'
    ]
  },
  keystone: {
    mounts: [
      'NpYYAkA5Z0uAElOjDg3Rag',
      'fSYv4j5-eEObwJEVhoBSvg',
      'x21sadthgE2FtDWSJKINGQ',
      'IxKwpY5d9EicYRRTMn8xVw',
      'JpOU9MgWXEmfLcQoAK4otA',
      'LCosRblddUCjPogeJ5eSeA',
      'hGeid9yk80GqIN5IUlp8aw'
    ],
    compartments: [
      'PlyaCcjDo0qu155JIZgm6A',
      'ytRGy84X_kW1odHYCBICDQ',
      'yM3Am4PqyUOIByTKzM6sPA',
      'H1ekC2-9c02VyNXY4IeLYA',
      'ln5c_ZvdCUSMVWZL_-m3yw',
      'H-oABJbYw0qgKd4QzU0WaA',
      'tCBEdcIUhk6AL-X6sCujsQ'
    ],
    modules: [
      'AWYvQUZ26k20yHOrVQKg6Q',
      'tmHTz6HkrE-RUtN94W6lyQ',
      'm5J1jQdOfUKM9r7NZjMpKA',
      'h29TSNGRo0m_DmeRr2BqDw',
      'E9sMCEquVk-NNj5heGmIlQ',
      'gx2UKetKWUm5BfZhiD5phQ',
      'ZJTmbGdjAk-XEKihIYHEuw'
    ]
  },
  vauxhall: {
    mounts: [
      'T9Ebo41iA0eBXNYx8uyisw',
      '2QQdxC4UE0KOM42r82-ETQ',
      'IFKM9E04aUaHS0IoNDMShA',
      'rX6hes7UqkyHjEyKbFaWTQ',
      'BRMwuusKC02YrbFXRrrNzA',
      'vO1oPhlSuUih_cdAZk3Hqg',
      'RLHQUFf200uLZjKX5axkMw',
      'uyPDg0tD3U6YKz18bVdkPg',
      'Xicf0TT7pEaFy_x1uk7ueQ',
      'XTg1H1Popku5gxW8sei5XQ'
    ],
    compartments: [
      'a8H-rPyVSk6uBOrP7lQYUA',
      'NBbnpHfpDUSS6bNgDlSjzw',
      'f_SO68qAGU-sw69T9GEWow',
      '9R0tfjsN-kiETUBLeCCmGw',
      '-bAF6R6aiU2Afn8T_oKY4g',
      '-7H3WGDwyESDI3lkHzBdGQ',
      'o1o5WOogUE255YVi43KlcQ',
      'DAcDeA0yskSjje92CY-F7w',
      'jKvCg9nYmkKfAn4-ht_Y6A'
    ],
    modules: [
      '8VKMmbA23Em1f-MBZKO2vA',
      'udQjNCWlA0awkv6LhH55uQ',
      '3eXWUksWXk-5q9MW52rghw',
      '_jqFwgf3EkKUUqHzVWidzw',
      'qulLVFUtzk2Qm8rZtfpFDw',
      'N_gGUWCQx0mUOX9RaFoI6A',
      'E1ZSbpYpZkufCyifYiiK8Q',
      '-hETYcmKH0eeXzkFpnhgeg',
      'hU2L93VfQU-jutnRC0dKgw'
    ]
  },
  axford: {
    mounts: [
      'whDP9rtbukKsocnrqnqaLQ',
      '8CtCKPLfZEOOFvm1DhV-oQ',
      'N7yRYYUuG0uOzd5aufgViA',
      'lYEQo4jJvEGxRqJa2MB25A',
      '9qreGP2Iw0KwuloNpNFTSg',
      'v9Sqa5DcCkibdi29AkakNg',
      '3MoeYUx1HUS6xNDBMsssig',
      'LbD9Txe-S0a_nODTqtrk7g',
      'KxV0hkkGBE2AOUCo5axQbg',
      'tyGXrucCjUe0FVyP1YDpUw',
      'rXZhdtbK3EC8K_cu6-r_Xg'
    ],
    compartments: [
      'ylUvmASKlEiyCtyCsNRKWg',
      'TX2TktZe7EWB9s-MqlDQgA',
      'cMs0g0g3WUeZJQASb7omew',
      'iHYg3DdYAUKS-uIBaRpNqA',
      'j2ST2yYfwk2_tBIayI3sQg',
      'SV_4BgcxwU24YIbep-k74g',
      'A8e_Z_d8WU2qzo6HTQwCBA',
      '2xmOS4ns_EaMYjBXrrhhig',
      'y1xLixKUzUSKT7Nz7aUkLQ',
      'RmUeePb-8EGbFaRgVjCXsA',
      'eYp1IPfupUGVITrkVhgZJw'
    ],
    modules: [
      'lxkgmNwqjUua0sRee2pDJg',
      '_Aun-HW-DkOGytVmYSn5IQ',
      '2ZLEJvxYf0CATl1lBi3cuQ',
      'ba-3VQ-zGEmDCfU12uaqaA',
      'hQ-Mzyns0UO4oMp7gG0d8A',
      'oyAxfnepkEG46VjtvQWXpw',
      'Yo9hPw6Mf060B3vqwSO4GA',
      'CgpRQa-660KEed1wTRL4vw',
      'awwdeDnNs0G-RceQF1TByA',
      'tnZD-B5Ls0qAyBtdz4yoTA',
      'S8ALcpOMYEqJog1v2q8UMQ',
      'rQWETsDGdE-AR8N3NPfwXg'
    ]
  },
  solomon: {
    mounts: [
      'vvpl_pV8B0W33ernrw31jg',
      'AznvpY2By0GiQxgeOTktww',
      'N5nMrtk4bkiSDzOffAmI1w',
      'HsGDhCR_T0KH1ddzUnSHmA',
      'oRZYmSFdsk2BFB03PqyS2w',
      'csaNZW85vkO8uIQ9XDe1iw',
      'eBG0UhrBiky4nUx81rPbLw',
      'M6NaSvLam06C-1DTggI99g',
      '3HKNGt305UC23fGT8OKcLw',
      'R_Y-LTCc0kydl6L8Sq5Dmw',
      '-gtO0aqGDEOH-Tm612wAoA',
      '37oZ894X7kG2Dq0E5mEKDQ',
      'ZjH6aPXnl0eOdklaPsRNAA',
      '4dNKr5i87EeDGq9coaWYJg'
    ],
    compartments: [
      'XAbGPXPgDEuDieQDqbA9MQ',
      'SX-a4J4eek2nmJztXePU3Q',
      '4sUWwP0VukOFbXT1YAMa9g',
      '09AenxIgokykwMTCMJz24w',
      'RtZe85sJUkO5b1xVdgPlqQ',
      '8YQv6H5fN0SylDc4p2fT-w',
      '6P8ohHGH_UaCAMEE5jFoRg',
      '80T00-71i0Wz-uNk04dzvQ',
      'GbGNS6ZFo0qx-dxuEmZTbg',
      'tkt9JPA2V0CWAO-Dfj8FHQ',
      'QkO76Z2PAUq8iD4cuc_P4A',
      'lp31cinWLE-ZGgtisuoaug',
      'wbgVupdnzEGle6f8tezZYQ',
      'Jl5HmssP406Au7n4NHceQw',
      '-wa5U_hdw0mHKI1PglDeGg',
      '97l_f_ktb0yoEquIa75icg'
    ],
    modules: [
      'Cz_cm8aRt0mRiYxdq26V7A',
      'eOvmJ625f0yGavYASYtYYQ',
      '1I5WRAX0oU6zIv7yEvqfmQ',
      '98aZYYnrXUWKXheUXHoLLw',
      'cVsLdxme_kGSU2jWlYnXzg',
      'rWWXZNnbKUi7djDPaKTttQ',
      'tgWD8fbOBEWq4KfvYDN_BQ',
      '4wfe6HbB9kCN7CDcF7_POQ',
      'yAg2wUCkJ0a6Ef3m96WyYw',
      'rTbYkycwWku-Ye1FCBJbvw',
      '-ao2fBk-zkK3QTdVH4fqHA',
      '5lOFQqLf-kOQq0zbIv0MKw',
      'PWWRnNTo60Cx9faCbUhx-g',
      '31ilgFi2f0ScvJbFia-Opg',
      'MjJFzcmp2kCLRWmjC30TKA'
    ]
  }
};

exports.componentNames = {
  mounts: [
    'CR10 Antenna',
    'CR70 Antenna',
    'E15 \'Masquerade\' Signature Booster',
    'E55 \'Spotlight\' Illuminator',
    'E57 \'Floodlight\' Illuminator',
    'E70 \'Interruption\' Jammer',
    'E71 \'Hangup\' Jammer',
    'E90 \'Blanket\' Jammer',
    'ES22 \'Pinard\' Electronic Support Module',
    'RF101 \'Bullseye\' Radar',
    'Mk610 Beam Turret',
    'MLS-2 Launcher',
    'MLS-3 Launcher',
    'VLS1-23 Launcher',
    'VLS2-16 Launcher',
    'Mk20 \'Defender\' PDT',
    'Mk25 \'Rebound\' PDT',
    'Mk90 \'Aurora\' PDT',
    'Mk61 Cannon',
    'Mk62 Cannon',
    'Mk64 Cannon',
    'Mk66 Cannon',
    'Mk68 Cannon',
    'Mk81 Railgun',
    'Mk82 Railgun',
    'Mk550 Mass Driver',
    'Mk600 Beam Cannon'
  ],
  compartments: [
    'Auxiliary Steering',
    'Basic CIC',
    'Citadel CIC',
    'Reinforced CIC',
    'Battle Dressing Station',
    'Berthing',
    'Damage Control Central',
    'Large DC Locker',
    'Rapid DC Locker',
    'Reinforced DC Locker',
    'Small DC Locker',
    'Small Workshop',
    'Plant Control Center',
    'Analysis Annex',
    'Intelligence Center',
    'Bulk Magazine',
    'Reinforced Magazine',
    'Gun Plotting Center'
  ],
  modules: [
    'Launcher Deluge System',
    'Magazine Sprinklers',
    'Supplementary Radio Amplifiers',
    'Signature Scrambler',
    'Redundant Reactor Failsafes',
    'Reinforced Thruster Nozzles',
    'Small Reactor Booster',
    'FR3300 Micro Reactor',
    'FR4800 Reactor',
    'FM200 Drive',
    'FM200R Drive',
    'FM230 \'Whiplash\' Drive',
    'FM240 \'Dragonfly\' Drive',
    'FM280 \'Raider\' Drive',
    'FM30X \'Prowler\' Drive',
    'FM500 Drive',
    'FM500R Drive',
    'FM530 \'Whiplash\' Drive',
    'FM540 \'Dragonfly\' Drive',
    'FM580 \'Raider\' Drive',
    'Adaptive Radar Receiver',
    'RM50 \'Parallax\' Radar',
    'RS35 \'Frontline\' Radar',
    'RS41 \'Spyglass\' Radar',
    'Track Correlator',
    'Actively Cooled Amplifiers',
    'Ammunition Elevators',
    'Energy Regulator',
    'Focused Particle Accelerator',
    'Mount Gyros',
    'Small Energy Regulator'
  ]
};

exports.munitionNames = [
  '120mm AP Shell',
  '120mm HE Shell',
  '120mm HE-RPF Shell',
  '20mm Slug',
  '250mm AP Shell',
  '250mm HE Shell',
  '250mm HE-RPF Shell',
  '300mm AP Rail Sabot',
  '450mm AP Shell',
  '450mm HE Shell',
  'Flak Round',
  'EA12 Chaff Decoy',
  'SDM-112 Riposte',
  'SGM-206 Thunderhead',
  'SGM-210 Squall',
  'SGM-225 Hurricane',
  'SGM-233 Gale',
  'SGT-350 Mace'
];

exports.missileBodyNames = [
  'SGM-1 Body'
];

exports.missileComponentNames = [
  'Command Receiver',
  'Fixed Active Radar Seeker',
  'Steerable Active Radar Seeker',
  'Steerable Extended Active Radar Seeker',
  'Fixed Semi-Active Radar Seeker',
  'Direct Guidance',
  'Blast Fragmentation'
];