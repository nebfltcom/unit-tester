const fs = require('fs');

//Copy default config if regular config not available
let data = JSON.parse(fs.readFileSync('config.default.json'));
if(fs.existsSync('config.json')) {
  data = Object.assign(data, JSON.parse(fs.readFileSync('config.json')));
}
fs.writeFileSync('config.json', JSON.stringify(data, null, 2));

module.exports = data;