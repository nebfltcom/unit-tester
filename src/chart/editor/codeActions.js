const serverUrl = window.location.origin;

window.currCodeChartName = null;
window.disableSendEdit = false;

const codeChartSelect = document.getElementById('code-chart-select');
const codeChartSelectPlaceholder = 'Select Chart...';
const codeNewButton = document.getElementById('code-new-button');
const codeSaveButton = document.getElementById('code-save-button');
const codeSaveAlert = document.getElementById('code-save-alert');

window.codeLoadChartListAction = async () => {
  let res = await fetch(`${serverUrl}/list`);
  let data = await res.json();
  codeChartSelect.innerHTML = `<option ${window.currCodeChartName === null ? 'selected' : ''}>${codeChartSelectPlaceholder}</option>`;
  data.forEach((chartName) => {
    codeChartSelect.innerHTML += `
    <option ${window.currCodeChartName === chartName ? 'selected' : ''} value="${chartName}">${chartName}</option`;
  });
}
window.codeLoadChartListAction();

window.codeLoadChartAction = async (chartName) => {
  let res = await fetch(`${serverUrl}/get`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ name: chartName })
  });
  let data = await res.json();
  window.currCodeChartName = chartName;
  if(data.tmp) {
    codeSaveButton.className ='btn btn-primary';
  }
  else {
    codeSaveButton.className ='btn btn-secondary disabled';
  }
  delete data.tmp;
  window.disableSendEdit = true;
  window.filterEditor.setValue(data.filterCode);
  window.transformEditor.setValue(data.transformCode);
  window.chartEditor.setValue(data.chartCode);
  window.setTimeout(() => { window.disableSendEdit = false; }, 100);
}
codeChartSelect.addEventListener('change', (event) => {
  let selectedChart = event.target.value;
  if(selectedChart !== codeChartSelectPlaceholder) {
    window.codeLoadChartAction(selectedChart);
  }
});

window.codeNewChartAction = async () => {
  let chartName = window.prompt('Please enter in the name of the new chart:');
  if(chartName !== null && chartName !== '') {
    let res = await fetch(`${serverUrl}/new`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ name: chartName })
    });
    await window.codeLoadChartAction(chartName);
    await window.codeLoadChartListAction();
  }
}
codeNewButton.addEventListener('click', () => {
  window.codeNewChartAction();
});

window.codeEditChartAction = async () => {
  let chart = {
    name: window.currCodeChartName,
    filterCode: window.filterEditor.getValue(),
    transformCode: window.transformEditor.getValue(),
    chartCode: window.chartEditor.getValue()
  };
  if(!window.disableSendEdit) {
    let res = await fetch(`${serverUrl}/edit`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ chart: chart })
    });
    codeSaveButton.className ='btn btn-primary';
  }
}

window.codeSaveChartAction = async (chartName) => {
  let res = await fetch(`${serverUrl}/save`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ name: chartName })
  });
  codeSaveButton.className ='btn btn-secondary disabled';
  await window.codeLoadChartAction(chartName);
  codeSaveAlert.style.display = 'block';
  window.setTimeout(() => {
    codeSaveAlert.style.display = 'none';
  }, 1000);
}
codeSaveButton.addEventListener('click', () => {
  window.codeSaveChartAction(window.currCodeChartName);
});