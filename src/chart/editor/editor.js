require.config({ paths: { 'vs': 'https://cdn.jsdelivr.net/npm/monaco-editor@0.34.0/min/vs' }});
window.MonacoEnvironment = { getWorkerUrl: () => proxy };

let proxy = URL.createObjectURL(new Blob([`
  self.MonacoEnvironment = {
    baseUrl: 'https://cdn.jsdelivr.net/npm/monaco-editor@0.34.0/min/'
  };
  importScripts('https://cdn.jsdelivr.net/npm/monaco-editor@0.34.0/min/vs/base/worker/workerMain.js');
`], { type: 'text/javascript' }));

require(['vs/editor/editor.main'], () => {
  let config = {
    automaticLayout: true,
    value: '',
    tabSize: 2,
    language: 'javascript',
    theme: 'vs-dark'
  };
  window.filterEditor = monaco.editor.create(document.getElementById('filterEditor'), config);
  window.filterEditor.getModel().onDidChangeContent(() => {
    window.codeEditChartAction();
  });
  window.transformEditor = monaco.editor.create(document.getElementById('transformEditor'), config);
  window.transformEditor.getModel().onDidChangeContent(() => {
    window.codeEditChartAction();
  });
  window.chartEditor = monaco.editor.create(document.getElementById('chartEditor'), config);
  window.chartEditor.getModel().onDidChangeContent(() => {
    window.codeEditChartAction();
  });
});

let resize = () => {
  let top = Math.max(
    document.getElementById('filterEditor').getBoundingClientRect().top,
    document.getElementById('transformEditor').getBoundingClientRect().top,
    document.getElementById('chartEditor').getBoundingClientRect().top
  );
  let height = window.innerHeight - top;
  document.getElementById('filterEditor').style.height = `${height}px`;
  document.getElementById('transformEditor').style.height = `${height}px`;
  document.getElementById('chartEditor').style.height = `${height}px`;
};
window.addEventListener('resize', resize);
document.getElementById('code-tab').addEventListener('click', () => {
  window.setTimeout(() => {
    resize();
  }, 500);
});
resize();