const express = require('express');
const fs = require('fs');
const path = require('path');
const chartManager = require('./chartManager');
const filter = require('./filter');
const transform = require('./transform');
const generator = require('./generator');

const app = express();
const port = 9997;
let currStatus = {};

//Creating data directories
try { fs.mkdirSync(path.resolve('./tmp')); } catch(err) {}
try { fs.mkdirSync(path.resolve('./tmp/results')); } catch(err) {}
try { fs.mkdirSync(path.resolve('./tmp/charts')); } catch(err) {}
try { fs.mkdirSync(path.resolve('./tmp/customtests')); } catch(err) {}
try { fs.mkdirSync(path.resolve('./tmp/data')); } catch(err) {}

app.use(express.json({ limit: '1tb' }));
app.use(express.static(path.join(__dirname, '/editor')));
app.get('/list', (req, res) => {
  //Send list of chart names
  res.send(chartManager.getChartNames());
});
app.post('/get', (req, res) => {
  //Send chart data (and indicate if it is tmp file)
  res.send(chartManager.getChart(req.body.name));
});
app.post('/new', (req, res) => {
  //Create new chart from name
  res.send(chartManager.newChart(req.body.name));
});
app.post('/edit', (req, res) => {
  //Save edits to tmp file
  res.send(chartManager.editChart(req.body.chart));
});
app.post('/save', (req, res) => {
  //Save edits from tmp file to real file
  res.send(chartManager.saveChart(req.body.name));
});
app.post('/run', async (req, res) => {
  //Run filter and transform code and send results
  let results = { html: '' };
  let chart = chartManager.getChart(req.body.name);
  delete chart.tmp;
  let filteredDataList = await filter.filterDataList([chart], (status) => {
    currStatus = status;
  }, 1000);
  currStatus = {};
  let transformedData = transform.transformData(chart, filteredDataList);
  results.html = generator.generateHtml(chart.chartCode, transformedData);
  res.send(results);
});
app.get('/status', (req, res) => {
  if(typeof currStatus.finished !== 'undefined') {
    res.send({
      finished: currStatus.finished,
      total: currStatus.total,
      filterRate: currStatus.filterRate,
      finishDateFromNow: currStatus.finishDate.fromNow(),
      finishDateFormat: currStatus.finishDate.format('dddd, MMMM Do YYYY, h:mm:ss a')
    });
  }
  else {
    res.send({});
  }
});

app.listen(port, () => {
  console.info(`[Chart Editor] NEBFLTCOM Automated Chart Editor is listening on port ${port}`);
  console.info(`[Chart Editor] Live Chart Editor is available at http://localhost:${port}`);
});