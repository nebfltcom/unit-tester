const fs = require('fs');
const path = require('path');
const md5 = require('md5');
const moment = require('moment');
const chartManager = require('./chartManager');

let filterCache = {};
let filterFuncs = {};

exports.filterDataList = async (charts, statusFunc = () => {}, statusInterval = 5000) => {
  let results = [];
  let dataList = fs.readdirSync(chartManager.dataDir).filter(e => e.includes('.json'));
  let startTime = Date.now();
  let lastUpdate = startTime;
  for(let i = 0;i < dataList.length;i++) {
    if(Date.now() > lastUpdate + statusInterval) {
      let runTime = Date.now() - startTime;
      let dataLeft = dataList.length - i;
      let filteredPerSecond = i / (runTime / 1000);
      let predictedDuration = dataLeft / filteredPerSecond;
      let predictedFinishDate = moment((predictedDuration * 1000) + Date.now());
      statusFunc({
        finished: i,
        total: dataList.length,
        filterRate: filteredPerSecond,
        finishDate: predictedFinishDate
      });
      lastUpdate = Date.now();
    }

    let dataPath = dataList[i];
    let data = JSON.parse(await fs.promises.readFile(path.resolve(`${chartManager.dataDir}/${dataPath}`)));
    for(let chart of charts) {
      await (async () => {
        let chartFilterFuncHash = md5(chart.filterCode);
        let dataHash = md5(data.name);
        if(typeof filterFuncs[chartFilterFuncHash] !== 'function') {
          filterFuncs[chartFilterFuncHash] = new Function('data', chart.filterCode);
        }
        if(typeof filterCache[chartFilterFuncHash] !== 'object') {
          filterCache[chartFilterFuncHash] = {};
        }
        if(typeof filterCache[chartFilterFuncHash][dataHash] === 'undefined') {
          filterCache[chartFilterFuncHash][dataHash] = null;
          if(filterFuncs[chartFilterFuncHash](data)) {
            let versions = [];
            for(let trial of data.trials) {
              if(!versions.includes(trial.version)) {
                versions.push(trial.version);
              }
            }
            filterCache[chartFilterFuncHash][dataHash] = {
              name: chart.name,
              path: dataPath,
              versions: versions
            };
            results.push(filterCache[chartFilterFuncHash][dataHash]);
          }
        }
        else {
          if(filterCache[chartFilterFuncHash][dataHash] !== null) {
            results.push(filterCache[chartFilterFuncHash][dataHash]);
          }
        }
      })();
    }
  }
  return results;
}