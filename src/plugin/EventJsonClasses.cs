namespace plugin {
  public class BaseEvent {
    public string EventName;
    public float Time;
    public string OriginShip;
  }

  public class ShipDataEvent : BaseEvent {
    public Vector3Data Position;
    public Vector3Data Rotation;
    public float CurrentHealth;
    public float MaxHealth;
    public string TrackedStatus;
    public bool IsBeingJammed;
    public bool TakingHits;
    public bool DriveDisabled;
    public bool PowerDisabled;
    public bool CommandDisabled;
    public bool IsEliminated;
    public string CommandStatus;
    public string CrewStatus;
    public int CurrentCrew;
    public int TotalCrew;
    public int ReserveCrew;
    public ComponentData[] Components;
    public TrackData[] Tracks;
  }

  public class ComponentData {
    public string ComponentName;
    public string ComponentID;
    public Vector3Data PositionInHull;
    public float CurrentHealth;
    public float MaxHealth;
    public int DebuffCount;
    public bool IsFunctional;
    public bool IsOnFire;
  }

  public class TrackData {
    public string TrackID;
    public string Mode;
    public bool IsPointDefenseTarget;
    public bool IsValid;
  }

  public class ComponentEvent : BaseEvent {
    public ComponentData Component;
  }

  public class FireBulletEvent : ComponentEvent {
    public string MunitionName;
    public Vector3Data FireDirection;
  }

  public class FireContinuousEvent : ComponentEvent {
    public bool IsStart;
    public Vector3Data FireDirection;
  }

  public class FireMissileEvent : ComponentEvent {
    public string MunitionName;
    public Vector3Data TargetPosition;
    public TrackData TargetTrack;
    public Vector3Data[] TargetPath;
  }

  public class DestroyMissileEvent : BaseEvent {
    public string MunitionName;
    public Vector3Data MissilePosition;
  }

  public class MunitionHitEvent : BaseEvent {
    public string MunitionName;
    public MunitionHitData MunitionHit;
  }

  public class ArmorDamageEvent : BaseEvent {
    public MunitionHitData MunitionHit;
    public bool DidRicochet;
    public float DamageDone;
  }

  public class ComponentDamageEvent: ComponentEvent {
    public MunitionHitData MunitionHit;
    public float DamageDone;
  }

  public class MunitionHitData {
    public Vector3Data Point;
    public Vector3Data LocalPoint;
    public Vector3Data Normal;
    public Vector3Data LocalNormal;
    public Vector3Data HitNormal;
    public Vector3Data LocalHitNormal;
  }

  public class Vector3Data {
    public float x;
    public float y;
    public float z;
  }

  public class Vector3IntData {
    public int x;
    public int y;
    public int z;
  }

  public class VersionData {
    public string Version;
  }
}