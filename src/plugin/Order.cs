using Cameras;
using HarmonyLib;
using System;
using Utility;
using UnityEngine;

namespace plugin {
  class OrderExecutor {
    public static void ExecuteOrder(Game.Units.ShipController shipController, ImprovedOrbitCamera camera, Order order) {
      //Execute orders here
      if(order.type == "TransformPosition") {
        shipController.transform.position = new Vector3(
          order.orderVector3data1.x / 10,
          order.orderVector3data1.y / 10,
          order.orderVector3data1.z / 10
        );
      }
      if(order.type == "TransformRotation") {
        shipController.transform.localRotation = Quaternion.Euler(
          order.orderVector3data1.x,
          order.orderVector3data1.y,
          order.orderVector3data1.z
        );
      }
      if(order.type == "TransformVelocity") {
        var _rigidbody = (Rigidbody)AccessTools.Field(typeof(Game.Units.ShipController), "_rigidbody").GetValue(shipController);
        _rigidbody.velocity = new Vector3(
          order.orderVector3data1.x / 10,
          order.orderVector3data1.y / 10,
          order.orderVector3data1.z / 10
        );
      }
      if(order.type == "WCON") {
        WeaponsControlStatus wconStatus;
        Enum.TryParse(order.orderStringData1, out wconStatus);
        shipController.SetWeaponsControl(wconStatus);
      }
      if(order.type == "EMCON") {
        var emconStatus = (
          (order.orderBoolData1 ? EmissionStatus.RadarOn : 0) |
          (order.orderBoolData1 ? EmissionStatus.CommsOn : 0) |
          (order.orderBoolData1 ? EmissionStatus.PassivesOn : 0)
        );
        shipController.SetEMCON(emconStatus);
      }
      if(order.type == "PDMode") {
        PointDefenseSettings pdMode = new PointDefenseSettings();
        pdMode.Turrets = (PDTurretMode)(Convert.ToInt32(order.orderBoolData1));
        Enum.TryParse(order.orderStringData1, out pdMode.Decoy);
        pdMode.MissileLastSalvo = order.orderBoolData5;
        pdMode.MissileSalvoCount = order.orderIntData1;
        pdMode.MissileSalvoSize = order.orderIntData2;
        Enum.TryParse(order.orderStringData2, out pdMode.Zone);
        AccessTools.Method(typeof(Game.Units.ShipController), "RpcChangePDMode").Invoke(shipController, new object[]{ pdMode });
      }
      if(order.type == "BattleShort") {
        shipController.SetBattleShort(order.orderBoolData1);
      }
      if(order.type == "MovementStyle") {
        MovementStyle movementStyle;
        Enum.TryParse(order.orderStringData1, out movementStyle);
        shipController.Network_moveStyle = movementStyle;
      }
      if(order.type == "CameraPosition") {
        camera.TweenToPosition(new Vector3(
          order.orderVector3data1.x / 10,
          order.orderVector3data1.y / 10,
          order.orderVector3data1.z / 10
        ));
      }
    }
  }
}
