using HarmonyLib;
using System;
using System.Collections.Generic;

namespace plugin {
  public class Recorder {
    public List<ShipDataEvent> Data = new List<ShipDataEvent>();
    private ShipDataEvent _lastAttackingShipData;
    private ShipDataEvent _lastTargetShipData;
    public void AddShipData(float time, Game.Units.ShipController shipController) {
      var components = shipController.Ship.Hull.AllComponents;
      var _structure = (Ships.HullStructure)AccessTools.Field(typeof(Ships.Hull), "_structure").GetValue(shipController.Ship.Hull);
      var _sensorContext = (Game.Sensors.SensorContext)AccessTools.Field(typeof(Game.Units.ShipController), "_sensorContext").GetValue(shipController);
      var _localTracks = (List<Game.Sensors.SensorTrack>)AccessTools.Field(typeof(Game.Sensors.SensorContext), "_localTracks").GetValue(_sensorContext);
      var currShipData = new ShipDataEvent();
      currShipData.EventName = "ShipDataEvent";
      currShipData.Time = time;
      currShipData.OriginShip = shipController.Ship.ShipNameNoPrefix;
      currShipData.Position = new Vector3Data();
      currShipData.Position.x = (float)Math.Round(shipController.transform.position.x * 10);
      currShipData.Position.y = (float)Math.Round(shipController.transform.position.y * 10);
      currShipData.Position.z = (float)Math.Round(shipController.transform.position.z * 10);
      currShipData.Rotation = new Vector3Data();
      currShipData.Rotation.x = (float)Math.Round(shipController.transform.rotation.eulerAngles.x);
      currShipData.Rotation.y = (float)Math.Round(shipController.transform.rotation.eulerAngles.y);
      currShipData.Rotation.z = (float)Math.Round(shipController.transform.rotation.eulerAngles.z);
      currShipData.CurrentHealth = _structure.CurrentHealth;
      currShipData.MaxHealth = (float)AccessTools.Field(typeof(Ships.HullStructure), "_maxHealth").GetValue(_structure);
      currShipData.TrackedStatus = shipController.BeingTracked.ToString();
      currShipData.IsBeingJammed = shipController.IsBeingJammed;
      currShipData.TakingHits = shipController.TakingHits;
      currShipData.DriveDisabled = shipController.Network_drivesDisabled;
      currShipData.PowerDisabled = shipController.Network_powerDisabled;
      currShipData.CommandDisabled = shipController.CommandDisabled;
      currShipData.IsEliminated = shipController.IsEliminated;
      currShipData.CommandStatus = shipController.CommandState.ToString();
      currShipData.CrewStatus = shipController.Network_crewStatusSummary.Status.ToString();
      currShipData.CurrentCrew = shipController.Network_crewStatusSummary.Current;
      currShipData.TotalCrew = shipController.Network_crewStatusSummary.AssignedBillets;
      currShipData.ReserveCrew = shipController.Network_crewStatusSummary.Reserve;
      currShipData.Components = new ComponentData[components.Length];
      for(int i = 0;i < components.Length;i++) {
        currShipData.Components[i] = Recorder.ExtractComponentData(components[i]);
      }
      currShipData.Tracks = new TrackData[_localTracks.Count];
      for(int i = 0;i < _localTracks.Count;i++) {
        currShipData.Tracks[i] = Recorder.ExtractTrackData(_localTracks[i]);
      }
      if(!this.IsEqual(currShipData)) {
        Data.Add(currShipData);
      }
    }
    public static ComponentData ExtractComponentData(Ships.HullComponent component) {
      var currComponentData = new ComponentData();
      var sockets = component.Socket.MyHull.AllSockets;
      currComponentData.ComponentName = component.ComponentName;
      currComponentData.ComponentID = "";
      if(component.Type == Ships.HullSocketType.Surface) {
        currComponentData.ComponentID += "Mount";
        var count = 0;
        foreach(var socket in sockets) {
          if(socket.Type == Ships.HullSocketType.Surface) {
            count++;
            if(socket.Key == component.Socket.Key) {
              currComponentData.ComponentID += count;
            }
          }
        }
      }
      if(component.Type == Ships.HullSocketType.Compartment) {
        currComponentData.ComponentID += "Compartment";
        var count = 0;
        foreach(var socket in sockets) {
          if(socket.Type == Ships.HullSocketType.Compartment) {
            count++;
            if(socket.Key == component.Socket.Key) {
              currComponentData.ComponentID += count;
            }
          }
        }
      }
      if(component.Type == Ships.HullSocketType.Module) {
        currComponentData.ComponentID += "Module";
        var count = 0;
        foreach(var socket in sockets) {
          if(socket.Type == Ships.HullSocketType.Module) {
            count++;
            if(socket.Key == component.Socket.Key) {
              currComponentData.ComponentID += count;
            }
          }
        }
      }
      currComponentData.PositionInHull = new Vector3Data();
      currComponentData.PositionInHull.x = component.PositionInHull.x;
      currComponentData.PositionInHull.y = component.PositionInHull.y;
      currComponentData.PositionInHull.z = component.PositionInHull.z;
      currComponentData.CurrentHealth = component.CurrentHealth;
      currComponentData.MaxHealth = component.MaxHealth;
      currComponentData.DebuffCount = component.DebuffCount;
      currComponentData.IsFunctional = component.IsFunctional;
      currComponentData.IsOnFire = component.IsOnFire();
      return currComponentData;
    }
    public static TrackData ExtractTrackData(Game.Sensors.ITrack track) {
      var currTrackData = new TrackData();
      currTrackData.TrackID = track.ID.ToString();
      currTrackData.Mode = track.Mode.ToString();
      currTrackData.IsPointDefenseTarget = track.IsPointDefenseTarget;
      currTrackData.IsValid = track.IsValid;
      return currTrackData;
    }
    public static MunitionHitData ExtractMunitionHitData(Munitions.MunitionHitInfo hitInfo) {
      var currMunitionHitData = new MunitionHitData();
      currMunitionHitData.Point = new Vector3Data();
      currMunitionHitData.Point.x = hitInfo.Point.x;
      currMunitionHitData.Point.y = hitInfo.Point.y;
      currMunitionHitData.Point.z = hitInfo.Point.z;
      currMunitionHitData.LocalPoint = new Vector3Data();
      currMunitionHitData.LocalPoint.x = hitInfo.LocalPoint.x;
      currMunitionHitData.LocalPoint.y = hitInfo.LocalPoint.y;
      currMunitionHitData.LocalPoint.z = hitInfo.LocalPoint.z;
      currMunitionHitData.Normal = new Vector3Data();
      currMunitionHitData.Normal.x = hitInfo.Normal.x;
      currMunitionHitData.Normal.y = hitInfo.Normal.y;
      currMunitionHitData.Normal.z = hitInfo.Normal.z;
      currMunitionHitData.LocalNormal = new Vector3Data();
      currMunitionHitData.LocalNormal.x = hitInfo.LocalNormal.x;
      currMunitionHitData.LocalNormal.y = hitInfo.LocalNormal.y;
      currMunitionHitData.LocalNormal.z = hitInfo.LocalNormal.z;
      currMunitionHitData.HitNormal = new Vector3Data();
      currMunitionHitData.HitNormal.x = hitInfo.HitNormal.x;
      currMunitionHitData.HitNormal.y = hitInfo.HitNormal.y;
      currMunitionHitData.HitNormal.z = hitInfo.HitNormal.z;
      currMunitionHitData.LocalHitNormal = new Vector3Data();
      currMunitionHitData.LocalHitNormal.x = hitInfo.LocalHitNormal.x;
      currMunitionHitData.LocalHitNormal.y = hitInfo.LocalHitNormal.y;
      currMunitionHitData.LocalHitNormal.z = hitInfo.LocalHitNormal.z;
      return currMunitionHitData;
    }
    private bool IsEqual(ShipDataEvent shipData) {
      var compareShipData = shipData.OriginShip == "Attacker" ? this._lastAttackingShipData : this._lastTargetShipData;
      var isEqual = true;
      if(compareShipData != null) {
        if(compareShipData.Components.Length == shipData.Components.Length) {
          for(int i = 0;i < compareShipData.Components.Length;i++) {
            if(!this.CompareComponent(compareShipData.Components[i],shipData.Components[i])) {
              isEqual = false;
            }
          }
        }
        else {
          isEqual = false;
        }
        if(compareShipData.Tracks.Length == shipData.Tracks.Length) {
          for(int i = 0;i < compareShipData.Tracks.Length;i++) {
            if(!this.CompareTrack(compareShipData.Tracks[i],shipData.Tracks[i])) {
              isEqual = false;
            }
          }
        }
        else {
          isEqual = false;
        }
        if(
          compareShipData.Position.x != shipData.Position.x ||
          compareShipData.Position.y != shipData.Position.y ||
          compareShipData.Position.z != shipData.Position.z ||
          compareShipData.Rotation.x != shipData.Rotation.x ||
          compareShipData.Rotation.y != shipData.Rotation.y ||
          compareShipData.Rotation.z != shipData.Rotation.z ||
          compareShipData.CurrentHealth != shipData.CurrentHealth ||
          compareShipData.MaxHealth != shipData.MaxHealth ||
          compareShipData.TrackedStatus != shipData.TrackedStatus ||
          compareShipData.IsBeingJammed != shipData.IsBeingJammed
        ) {
          isEqual = false;
        }
      }
      else {
        isEqual = false;
      }
      if(shipData.OriginShip == "Attacker") {
        this._lastAttackingShipData = shipData;
      }
      else {
        this._lastTargetShipData = shipData;
      }
      return isEqual;
    }
    private bool CompareComponent(ComponentData c1, ComponentData c2) {
      return (
        c1.ComponentName == c2.ComponentName &&
        c1.ComponentID == c2.ComponentID &&
        c1.PositionInHull.x == c2.PositionInHull.x &&
        c1.PositionInHull.y == c2.PositionInHull.y &&
        c1.PositionInHull.z == c2.PositionInHull.z &&
        c1.CurrentHealth == c2.CurrentHealth &&
        c1.MaxHealth == c2.MaxHealth &&
        c1.DebuffCount == c2.DebuffCount &&
        c1.IsFunctional == c2.IsFunctional &&
        c1.IsOnFire == c2.IsOnFire
      );
    }
    private bool CompareTrack(TrackData t1, TrackData t2) {
      return (
        t1.TrackID == t2.TrackID &&
        t1.Mode == t2.Mode &&
        t1.IsPointDefenseTarget == t2.IsPointDefenseTarget &&
        t1.IsValid == t2.IsValid
      );
    }
  }
}