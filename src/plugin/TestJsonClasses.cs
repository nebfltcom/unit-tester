namespace plugin {
  public class Test {
    public string name;
    public string type;
    public int testAmount;
    public float maxTime;
    public float timeScale;
    public string description;
    public bool cameraFollow;
    public Setup setup;
  }

  public class Setup {
    public Ship attackingShip;
    public Ship targetShip;
  }

  public class Ship {
    public Position position;
    public Rotation rotation;
    public Velocity velocity;
    public CircleMove circleMove; 
    public Order[] orders;
  }

  public class Position {
    public float x;
    public float y;
    public float z;
    public bool isStatic;
  }

  public class Rotation {
    public float rx;
    public float ry;
    public float rz;
    public bool isStatic;
  }

  public class Velocity {
    public float vx;
    public float vy;
    public float vz;
    public bool isStatic;
  }

  public class CircleMove {
    public bool enable;
    public float targetRange;
    public float targetVelocity;
  }

  public class Order {
    public string type;
    public float time;
    public string orderStringData1;
    public string orderStringData2;
    public string orderStringData3;
    public string orderStringData4;
    public string orderStringData5;
    public int orderIntData1;
    public int orderIntData2;
    public int orderIntData3;
    public int orderIntData4;
    public int orderIntData5;
    public float orderFloatData1;
    public float orderFloatData2;
    public float orderFloatData3;
    public float orderFloatData4;
    public float orderFloatData5;
    public bool orderBoolData1;
    public bool orderBoolData2;
    public bool orderBoolData3;
    public bool orderBoolData4;
    public bool orderBoolData5;
    public Vector3IntData orderVector3Intdata1;
    public Vector3IntData orderVector3Intdata2;
    public Vector3IntData orderVector3Intdata3;
    public Vector3IntData orderVector3Intdata4;
    public Vector3IntData orderVector3Intdata5;
    public Vector3Data orderVector3data1;
    public Vector3Data orderVector3data2;
    public Vector3Data orderVector3data3;
    public Vector3Data orderVector3data4;
    public Vector3Data orderVector3data5;
  }
}