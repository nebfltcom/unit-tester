using System.Collections.Generic;

namespace plugin {
  public class EventBuffer {
    private static EventBuffer _instance = (EventBuffer)null;
    public static EventBuffer Instance {
      get {
        if(EventBuffer._instance == null) {
          EventBuffer._instance = new EventBuffer();
        }
        return EventBuffer._instance;
      }
    }

    public bool Enabled = false;
    public List<BaseEvent> Data = new List<BaseEvent>();

    public void Reset() {
      this.Data.Clear();
      this.Enabled = false;
    }

    public void AddData(BaseEvent data) {
      if(this.Enabled) {
        this.Data.Add(data);
      }
    }

    public void AddShipData(List<ShipDataEvent> shipData) {
      if(this.Enabled) {
        foreach(var data in shipData) {
          this.Data.Add(data);
        }
      }
    }
  }
}