const child_process = require('child_process');
const fetch = require('sync-fetch');
const fs = require('fs-extra');
const path = require('path');
const { unzip } = require('cross-unzip');
const { getGamePath } = require('steam-game-path');

(async () => {
  //Clear gamedir directory
  console.info(`[Plugin Setup] Clearing ./tmp/gamedir`);
  try { fs.rmSync(path.resolve('./tmp/gamedir'), { recursive: true, force: true }); } catch(err) {}
  try { fs.mkdirSync(path.resolve('./tmp/gamedir')); } catch(err) {}
  
  //Get steam game path
  console.info(`[Plugin Setup] Searching for game in Steam`);
  let gamePathObject = getGamePath(887570, true);
  let gamePathString = path.resolve(gamePathObject.game.path);

  //Copy steam game into gamedir
  console.info(`[Plugin Setup] Copying Steam game files into ./tmp/gamedir`);
  try{ fs.copySync(gamePathString, path.resolve('./tmp/gamedir')); } catch(err) {}

  //Download Goldberg Emulator
  console.info(`[Plugin Setup] Downloading Goldberg Emulator`);
  fs.writeFileSync(path.resolve('./tmp/gamedir/goldbergemu.zip'), fetch('https://gitlab.com/Mr_Goldberg/goldberg_emulator/-/jobs/2975828369/artifacts/download').buffer());

  //Unzip Goldberg Emulator
  console.info(`[Plugin Setup] Unzipping Goldberg Emulator`);
  try { fs.mkdirSync(path.resolve('./tmp/gamedir/goldbergemu')); } catch(err) {}
  await new Promise((resolve) => {
    unzip(path.resolve('./tmp/gamedir/goldbergemu.zip'), path.resolve('./tmp/gamedir/goldbergemu'), () => {
      resolve();
    });
  });

  //Copy Goldberg Emulator DLL
  console.info(`[Plugin Setup] Copying Goldberg Emulator DLL`);
  try{ fs.copyFileSync(path.resolve('./tmp/gamedir/goldbergemu/steam_api64.dll'), path.resolve('./tmp/gamedir/Nebulous_Data/Plugins/x86_64/steam_api64.dll')); } catch(err) {}
  try { fs.rmSync(path.resolve('./tmp/gamedir/goldbergemu'), { recursive: true, force: true }); } catch(err) {}

  //Download BepInEx
  console.info(`[Plugin Setup] Downloading BepInEx`);
  fs.writeFileSync(path.resolve('./tmp/gamedir/bepinex.zip'), fetch('https://github.com/BepInEx/BepInEx/releases/download/v5.4.19/BepInEx_x64_5.4.19.0.zip').buffer());

  //Unzip BepInEx
  console.info(`[Plugin Setup] Unzipping BepInEx`);
  await new Promise((resolve) => {
    unzip(path.resolve('./tmp/gamedir/bepinex.zip'), path.resolve('./tmp/gamedir'), () => {
      resolve();
    });
  });

  //Run Nebulous to create BepInEx folders
  console.info(`[Plugin Setup] First time BepInEx setup`);
  let gameProcess = child_process.spawn('Nebulous.exe', ['-batchmode'], {
    cwd: path.resolve('./tmp/gamedir')
  });
  while(!fs.existsSync(path.resolve('./tmp/gamedir/BepInEx/plugins'))) {
    await new Promise((resolve) => { setTimeout(resolve, 1000); });
  }
  gameProcess.kill('SIGKILL');

  //Copy game DLLs
  console.info(`[Plugin Setup] Copying game DLLs`);
  try { fs.rmSync(path.resolve('./src/plugin/lib'), { recursive: true, force: true }); } catch(err) {}
  try { fs.mkdirSync(path.resolve('./src/plugin/lib')); } catch(err) {}
  try { fs.rmSync(path.resolve('./src/plugin/obj'), { recursive: true, force: true }); } catch(err) {}
  try { fs.mkdirSync(path.resolve('./src/plugin/obj')); } catch(err) {}
  try{ fs.copySync(path.resolve('./tmp/gamedir/Nebulous_Data/Managed'), path.resolve('./src/plugin/lib')); } catch(err) {}

  //Build plugin
  try { fs.rmSync(path.resolve('./src/plugin/bin'), { recursive: true, force: true }); } catch(err) {}
  try { fs.mkdirSync(path.resolve('./src/plugin/bin')); } catch(err) {}
  console.info(`[Plugin Setup] Building plugin`);
  let buildProcess = child_process.spawnSync('dotnet.exe', ['build'], {
    cwd: path.resolve('./src/plugin')
  });
  let buildOutput = buildProcess.stdout.toString();
  console.info(buildOutput);
  if(!buildOutput.includes('Build succeeded')) {
    throw 'Plugin build failed!';
  }

  //Copy plugin DLLs
  console.info(`[Plugin Setup] Copying plugin DLLs`);
  try{ fs.copyFileSync(path.resolve('./src/plugin/bin/Debug/net46/autotester.dll'), path.resolve('./tmp/gamedir/BepInEx/plugins/autotester.dll')); } catch(err) {}
  try{ fs.copyFileSync(path.resolve('./src/plugin/bin/Debug/net46/autotester.pdb'), path.resolve('./tmp/gamedir/BepInEx/plugins/autotester.pdb')); } catch(err) {}
  try{ fs.copyFileSync(path.resolve('./src/plugin/bin/Debug/net46/Newtonsoft.Json.dll'), path.resolve('./tmp/gamedir/BepInEx/plugins/Newtonsoft.Json.dll')); } catch(err) {}

  //Copy serialization DLL
  console.info(`[Plugin Setup] Copying serialization DLL`);
  try{ fs.copyFileSync(path.resolve('C:/Program Files (x86)/Reference Assemblies/Microsoft/Framework/v3.0/System.Runtime.Serialization.dll'), path.resolve('./tmp/gamedir/BepInEx/plugins/System.Runtime.Serialization.dll')); } catch(err) {}
})();
