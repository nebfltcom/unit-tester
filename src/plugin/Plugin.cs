using BepInEx;
using Cameras;
using FleetEditor;
using Game;
using Game.AI;
using Game.AI.StratPolicies;
using Game.Units;
using HarmonyLib;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using UnityEngine;
using UnityEngine.Networking;
using UI;
using Utility;

namespace plugin {
  [BepInPlugin(PluginInfo.PLUGIN_GUID, PluginInfo.PLUGIN_NAME, PluginInfo.PLUGIN_VERSION)]
  [BepInProcess("Nebulous.exe")]
  public class Plugin : BaseUnityPlugin {
    private string _testerId = "";
    private int _phase = 0;
    private float _lastUpdate = 0;
    private float _updateInterval = 10.0f;
    private float _startTime = 0;
    private ImprovedOrbitCamera _camera = null;
    private ShipController _attackingShip = null;
    private int _attackingShipOrderIndex = 0;
    private ShipController _targetShip = null;
    private int _targetShipOrderIndex = 0;
    private StrategyTable _botStrategy = null;
    private Test _currTest = null;
    private Recorder _currRecorder = new Recorder();
    private void Awake() {
      // Plugin startup logic
      Logger.LogInfo($"Automated Tester plugin loaded!");
      //Grabbing tester id
      string[] args = System.Environment.GetCommandLineArgs();
      for(int i = 0;i < args.Length;i++) {
        if(args[i] == "-tester-id") {
          this._testerId = args [i + 1];
        }
      }
      //Create bot strat table
      this._botStrategy = ScriptableObject.CreateInstance<StrategyTable>();
      this._botStrategy.name = "FireAllAI";
      this._botStrategy.AttackStylePolicy = AttackStyle.SlugOut;
      this._botStrategy.WoundedTargetingStyle = TargetingStyle.FocusFire;
      this._botStrategy.WeaponDivisionPolicy = WeaponDivision.FocusAll;
      this._botStrategy.JammingPolicy = JammingUsage.Always;
      this._botStrategy.EngagementRangePolicy = EngagementRange.Long;
      Harmony.CreateAndPatchAll(typeof(Patch));
      Harmony.CreateAndPatchAll(typeof(SettingsOverride));
      SettingsOverride.Init();
    }

    private void Update() {
      if(this._lastUpdate + this._updateInterval < Time.time) {
        this._lastUpdate = Time.time;
        if(this._phase == 0) {
          //Check for main menu
          var foundMainMenu = FindObjectsOfType<UI.MainMenu>();
          if(foundMainMenu.Length > 0) {
            Logger.LogInfo($"Found Main Menu, activating Skirmish Menu");
            var mainMenu = foundMainMenu[0];
            mainMenu.SkirmishButton();
            //Change update interval to 1 second
            this._updateInterval = 1;
            this._phase++;
          }
        }
        if(this._phase == 1) {
          //Check for test from server and send game version
          if(this._currTest == null) {
            StartCoroutine(GetTest($"http://localhost:9999/test/{this._testerId}", this.SaveTest));
            var versionData = new VersionData();
            versionData.Version = Application.version;
            var json = JsonConvert.SerializeObject(versionData);
            Logger.LogInfo($"Serialized JSON for game version data");

            UnityWebRequest webRequest = UnityWebRequest.Put($"http://localhost:9999/version/{this._testerId}", json);
            webRequest.SetRequestHeader("Content-Type", "application/json");
            webRequest.SendWebRequest();
          }
          else {
            this._phase++;
          }
        }
        if(this._phase == 2) {
          //Check for player slots
          var foundSlots = FindObjectsOfType<UI.SkirmishLobbyPlayerSlot>();
          if(foundSlots.Length > 0) {
            Logger.LogInfo($"Found Player Slots in Skirmish Menu");
            SkirmishLobbyPlayerSlot botSlot = null;
            SkirmishLobbyPlayerSlot playerSlot = null;
            foreach(var slot in foundSlots) {
              if(slot.Player != null) {
                if(slot.Player.BotPersonality == null) {
                  playerSlot = slot;
                }
                else {
                  botSlot = slot;
                }
              }
            }
            botSlot.Player.BotPersonality = this._botStrategy;
            playerSlot.OpenFleetSelect();
            this._phase++;
          }
        }
        if(this._phase == 3) {
          //Check for fleet list items and modal menus
          var foundFleetListItems = FindObjectsOfType<FleetEditor.FleetListItem>();
          var foundFleetListModal = FindObjectsOfType<UI.ModalFleetList>();
          if(foundFleetListItems.Length > 0 && foundFleetListModal.Length > 0) {
            Logger.LogInfo($"Found Fleet List Item in Fleet Selection");
            foreach(var modal in foundFleetListModal) {
              foreach(var item in foundFleetListItems) {
                if(item.Name == "Target" && modal.IsOpen) {
                  item.Clicked();
                  modal.Confirm();
                  this._phase++;
                }
              }
            }
          }
        }
        if(this._phase == 4) {
          //Check for lobby settings pane
          var foundSettingsPane = FindObjectsOfType<UI.SkirmishLobbySettingsPane>();
          if(foundSettingsPane.Length > 0) {
            Logger.LogInfo($"Found Settings Pane in Skirmish Menu");
            foreach(var settings in foundSettingsPane) {
              if(settings.name == "Settings") {
                settings.DropdownSelectMap(6);
                var _gameSettings = (Game.SkirmishGameSettings)AccessTools.Field(typeof(SkirmishLobbySettingsPane), "_gameSettings").GetValue(settings);
                var _allScenarios = (List<Missions.ScenarioGraph>)AccessTools.Field(typeof(Game.SkirmishGameSettings), "_allScenarios").GetValue(_gameSettings);
                foreach(var scenario in _allScenarios) {
                  if(scenario.ScenarioName == "Annihilation") {
                    AccessTools.Field(typeof(Game.SkirmishGameSettings), "_selectedScenario").SetValue(_gameSettings, scenario);
                  }
                }
                this._phase++;
              }
            }
          }
        }
        if(this._phase == 5) {
          //Check for player slots
          var foundSkirmishMenu = FindObjectsOfType<UI.SkirmishLobbyMenu>();
          if(foundSkirmishMenu.Length > 0) {
            Logger.LogInfo($"Starting Game in Skirmish Menu");
            var skirmishMenu = foundSkirmishMenu[0];
            skirmishMenu.LaunchGame();
            this._phase++;
          }
        }
        if(this._phase == 6) {
          //Check for both attacking and target ship controllers
          var foundShipControllers = FindObjectsOfType<Game.Units.ShipController>();
          var foundCameras = FindObjectsOfType<Cameras.ImprovedOrbitCamera>();
          if(foundShipControllers.Length > 0 && foundCameras.Length > 0) {
            foreach(var shipController in foundShipControllers) {
              if(shipController.Ship.ShipNameNoPrefix == "Attacker") {
                this._attackingShip = shipController;
              }
              if(shipController.Ship.ShipNameNoPrefix == "Target") {
                this._targetShip = shipController;
              }
            }
            this._camera = foundCameras[0];
            if(this._attackingShip != null && this._targetShip != null) {
              Logger.LogInfo($"Found both ships, starting setup phase");
              //Disable update interval
              this._updateInterval = -1.0f;
              this._phase++;
            }
          }
        }
        if(this._phase == 7) {
          //Setup phase
          this._startTime = Time.time;
          this._attackingShip.transform.position = new Vector3(
            this._currTest.setup.attackingShip.position.x / 10,
            this._currTest.setup.attackingShip.position.y / 10,
            this._currTest.setup.attackingShip.position.z / 10
          );
          this._attackingShip.transform.localRotation = Quaternion.Euler(
            this._currTest.setup.attackingShip.rotation.rx,
            this._currTest.setup.attackingShip.rotation.ry,
            this._currTest.setup.attackingShip.rotation.rz
          );
          var _attackingRigidbody = (Rigidbody)AccessTools.Field(typeof(Game.Units.ShipController), "_rigidbody").GetValue(this._attackingShip);
          _attackingRigidbody.velocity = new Vector3(
            this._currTest.setup.attackingShip.velocity.vx / 10,
            this._currTest.setup.attackingShip.velocity.vy / 10,
            this._currTest.setup.attackingShip.velocity.vz / 10
          );
          this._attackingShip.SetWeaponsControl(WeaponsControlStatus.Free);
          this._targetShip.transform.position = new Vector3(
            this._currTest.setup.targetShip.position.x / 10,
            this._currTest.setup.targetShip.position.y / 10,
            this._currTest.setup.targetShip.position.z / 10
          );
          this._targetShip.transform.localRotation = Quaternion.Euler(
            this._currTest.setup.targetShip.rotation.rx,
            this._currTest.setup.targetShip.rotation.ry,
            this._currTest.setup.targetShip.rotation.rz
          );
          var _targetRigidbody = (Rigidbody)AccessTools.Field(typeof(Game.Units.ShipController), "_rigidbody").GetValue(this._targetShip);
          _targetRigidbody.velocity = new Vector3(
            this._currTest.setup.targetShip.velocity.vx / 10,
            this._currTest.setup.targetShip.velocity.vy / 10,
            this._currTest.setup.targetShip.velocity.vz / 10
          );
          this._camera.TweenToPosition(new Vector3(
            this._currTest.setup.targetShip.position.x / 10,
            this._currTest.setup.targetShip.position.y / 10,
            this._currTest.setup.targetShip.position.z / 10
          ));
          this._targetShip.SetWeaponsControl(WeaponsControlStatus.Free);
          var attackingTU = this._attackingShip.AIController.AssignedTU;
          typeof(Game.AI.TaskUnit).GetProperty("CurrentAssignment").SetValue(attackingTU, Mission.Attack);
          Time.timeScale = this._currTest.timeScale;
          Time.maximumDeltaTime = (float)(1.25 / (60 * this._currTest.timeScale));
          EventBuffer.Instance.Enabled = true;
          Logger.LogInfo($"Finished setup, starting recording phase");
          //Reenable update interval (set to below 1/60 for full fps)
          this._updateInterval = 0.01f;
          this._phase++;
        }
        if(this._phase == 8) {
          if(this._targetShip != null && this._attackingShip != null) {
            //Recording phase
            //Disable developer console
            Debug.developerConsoleVisible = false;
            //Control position, rotation, and velocity of ships if needed by test
            if(this._currTest.setup.attackingShip.position.isStatic) {
              this._attackingShip.transform.position = new Vector3(
                this._currTest.setup.attackingShip.position.x / 10,
                this._currTest.setup.attackingShip.position.y / 10,
                this._currTest.setup.attackingShip.position.z / 10
              );
            }
            if(this._currTest.setup.attackingShip.rotation.isStatic) {
              this._attackingShip.transform.localRotation = Quaternion.Euler(
                this._currTest.setup.attackingShip.rotation.rx,
                this._currTest.setup.attackingShip.rotation.ry,
                this._currTest.setup.attackingShip.rotation.rz
              );
            }
            if(this._currTest.setup.attackingShip.velocity.isStatic) {
              var _attackingRigidbody = (Rigidbody)AccessTools.Field(typeof(Game.Units.ShipController), "_rigidbody").GetValue(this._attackingShip);
              _attackingRigidbody.velocity = new Vector3(
                this._currTest.setup.attackingShip.velocity.vx / 10,
                this._currTest.setup.attackingShip.velocity.vy / 10,
                this._currTest.setup.attackingShip.velocity.vz / 10
              );
            }
            if(this._currTest.setup.targetShip.position.isStatic) {
              this._targetShip.transform.position = new Vector3(
                this._currTest.setup.targetShip.position.x / 10,
                this._currTest.setup.targetShip.position.y / 10,
                this._currTest.setup.targetShip.position.z / 10
              );
            }
            if(this._currTest.setup.targetShip.rotation.isStatic) {
              this._targetShip.transform.localRotation = Quaternion.Euler(
                this._currTest.setup.targetShip.rotation.rx,
                this._currTest.setup.targetShip.rotation.ry,
                this._currTest.setup.targetShip.rotation.rz
              );
            }
            if(this._currTest.setup.targetShip.velocity.isStatic) {
              var _targetRigidbody = (Rigidbody)AccessTools.Field(typeof(Game.Units.ShipController), "_rigidbody").GetValue(this._targetShip);
              _targetRigidbody.velocity = new Vector3(
                this._currTest.setup.targetShip.velocity.vx / 10,
                this._currTest.setup.targetShip.velocity.vy / 10,
                this._currTest.setup.targetShip.velocity.vz / 10
              );
            }
            //Execute ship orders if needed
            var doneExecuting = false;
            while(!doneExecuting && this._targetShipOrderIndex < this._currTest.setup.targetShip.orders.Length) {
              var currOrder = this._currTest.setup.targetShip.orders[this._targetShipOrderIndex];
              if(Time.time - this._startTime >= currOrder.time) {
                OrderExecutor.ExecuteOrder(this._targetShip, this._camera, currOrder);
                this._targetShipOrderIndex++;
              }
              else {
                doneExecuting = true;
              }
            }
            doneExecuting = false;
            while(!doneExecuting && this._attackingShipOrderIndex < this._currTest.setup.attackingShip.orders.Length) {
              var currOrder = this._currTest.setup.attackingShip.orders[this._attackingShipOrderIndex];
              if(Time.time - this._startTime >= currOrder.time) {
                OrderExecutor.ExecuteOrder(this._attackingShip, this._camera, currOrder);
                this._attackingShipOrderIndex++;
              }
              else {
                doneExecuting = true;
              }
            }
            //Camera follow if enabled
            if(this._currTest.cameraFollow) {
              this._camera.TweenToPosition(this._targetShip.transform.position + new Vector3(0,7,0), 0.1f);
              this._camera.SetLookDirection(this._attackingShip.transform.position - this._camera.CameraTruckPosition + new Vector3(0,-25,0), 0);
              AccessTools.Method(typeof(Cameras.ImprovedOrbitCamera), "ZoomAbsolute").Invoke(this._camera, new object[]{ 75f });
            }
            //Record ship data
            this._currRecorder.AddShipData(Time.time, this._attackingShip);
            this._currRecorder.AddShipData(Time.time, this._targetShip);
            //End test if needed
            if(Time.time - this._startTime > this._currTest.maxTime) {
              var player = this._targetShip.OwnedBy;
              if(!player.HasRetired) {
                ((Game.SkirmishPlayer)player).Retire();
              }
            }
            //Quit if way past max test time and match has not ended yet
            if(Time.time - this._startTime > (this._currTest.maxTime + 60)) {
              Application.Quit();
            }
          }
          else {
            //Recording finished
            Logger.LogInfo($"Finished recording, waiting for Battle Report Menu");
            Time.timeScale = 1;
            Time.maximumDeltaTime = (float)(1.25 / 60);
            //Sending recorder data
            EventBuffer.Instance.AddShipData(this._currRecorder.Data);
            var json = JsonConvert.SerializeObject(EventBuffer.Instance.Data);
            UnityWebRequest webRequest = UnityWebRequest.Put($"http://localhost:9999/result/{this._testerId}", json);
            webRequest.SetRequestHeader("Content-Type", "application/json");
            webRequest.SendWebRequest();
            //Change update interval to 1 second
            this._updateInterval = 1;
            this._phase++;
          }
        }
        if(this._phase == 9) {
          //Check for main menu
          var foundBattleReportMenu = FindObjectsOfType<Game.UI.BattleReport.BattleReportMenu>();
          if(foundBattleReportMenu.Length > 0) {
            Logger.LogInfo($"Found Battle Report Menu, returning to Main Menu");
            var battleReportMenu = foundBattleReportMenu[0];
            battleReportMenu.ReturnToMainMenu();
            //Reset test state
            this._camera = null;
            this._startTime = 0;
            this._attackingShip = null;
            this._attackingShipOrderIndex = 0;
            this._targetShip = null;
            this._targetShipOrderIndex = 0;
            this._currTest = null;
            this._currRecorder = new Recorder();
            EventBuffer.Instance.Reset();
            //Return to first phase to execute next test
            this._phase = 0;
          } 
        }
      }
    }
    private void SaveTest(string json) {
      var test = JsonConvert.DeserializeObject<Test>(json);
      this._currTest = test;
    }
    private IEnumerator GetTest(string url, Action<string> pathCallback) {
      UnityWebRequest webRequest = UnityWebRequest.Get(url);
      //Request and wait for the desired string
      yield return webRequest.SendWebRequest();

      if(webRequest.result != UnityWebRequest.Result.Success) {
        //Server is down, quitting
        Logger.LogInfo(webRequest.error);
        Application.Quit();
      }
      else {
        //Save test
        pathCallback(webRequest.downloadHandler.text);
      }
    }
  }
}
