const { XMLBuilder } = require('fast-xml-parser');
const { v4 } = require('uuid');
const deepcopy = require('deepcopy');
const dict = require('../common/dict');
const shipBuilder = new XMLBuilder({
  format: true,
  ignoreAttributes: false,
  suppressBooleanAttributes: false,
  suppressEmptyNode: true
});

const baseShip = {
  '?xml': {
    '@_version': '1.0'
  },
  Fleet: {
    '@_xmlns:xsd': 'http://www.w3.org/2001/XMLSchema',
    '@_xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
    Name: '',
    Version: 3,
    TotalPoints: 0,
    FactionKey: 'Stock/Alliance',
    Ships: {
      Ship: {
        SaveID: {
          '@_xsi:nil': 'true'
        },
        Key: '',
        Name: '',
        Cost: 0,
        Callsign: '',
        Number: 0,
        SymbolOption: 0,
        HullType: '',
        SocketMap: {
          HullSocket: []
        }
      }
    },
    MissileTypes: {
      MissileTemplate: []
    }
  }
};

const baseMissile = {
  prefix: 'SGM-222',
  name: 'Hurricane',
  body: 'SGM-2 Body',
  components: [
    {
      size: 1,
      type: 'CommandSeeker',
      name: 'Command Receiver',
      settings: {
        Mode: 'Targeting'
      }
    },
    {
      size: 1
    },
    {
      size: 1,
      type: 'CruiseGuidance',
      name: 'Cruise Guidance',
      settings: {
        Role: 'Offensive',
        HotLaunch: false,
        SelfDestructOnLost: false,
        Maneuvers: 'None',
        DefensiveDoctrine: {
          TargetSizeMask: 12,
          TargetSizeOrdering: 'Descending',
          FarthestFirst: false
        }
      }
    },
    {
      size: 5,
      type: null,
      name: 'HE Impact'
    },
    {
      size: 5,
      type: 'MissileEngine',
      name: null,
      settings: {
        BalanceValues: {
          A: 0.496714979,
          B: 0.4663903,
          C: 0.03689471
        }
      }
    }
  ]
}

exports.randomKey = () => {
  let ret = '';
  const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_-';
  for(let i = 0;i < 22;i++) {
    ret += chars.charAt(Math.floor(Math.random() * chars.length));
  }
  return ret;
}

/*
Component JSON:
{
  componentName: string,
  componentData: [
    {
      munitionName: string,
      quantity: number
    }
  ]
}
Missile JSON:
{
  prefix: string,
  name: string,
  body: string,
  components: [
    {
      size: number,
      type: string,
      name: string,
      settings: {
        //Type: ActiveSeeker
        Mode: string,
        DetectPDTargets: boolean,
        //Type: DirectGuidance
        ComponentKey: string,
        Role: string,
        HotLaunch: boolean
        SelfDestructOnLost: boolean,
        Maneuvers: string,
        ApproachAngleControl: boolean
        //Type: MissileEngine
        BalanceValues: {
          A: number, //Top Speed
          B: number, //Burn Duration
          C: number  //Maneuverability
        }
      }
    }
  ]
}
*/
exports.generateShip = (name, hullType, mounts, compartments, modules, missiles = [ baseMissile ]) => {
  let ret = deepcopy(baseShip);
  ret.Fleet.Name = name;
  ret.Fleet.Ships.Ship.Name = name;
  ret.Fleet.Ships.Ship.Callsign = name;
  ret.Fleet.Ships.Ship.Key = v4();
  ret.Fleet.Ships.Ship.Number = Math.round(Math.random() * 1000);
  ret.Fleet.Ships.Ship.HullType = dict.hullTypeName[hullType];
  for(let i = 0;i < mounts.length;i++) {
    if(mounts[i] !== null) {
      let currMount = mounts[i];
      let currRet = {
        Key: dict.hullTypeSocketKeys[hullType].mounts[i],
        ComponentName: `Stock/${currMount.componentName}`
      };
      if(currMount.componentName.includes('VLS')) {
        currRet.ComponentData = {
          '@_xsi:type': currMount.componentName.includes('VLS-1') ? 'CellLauncherData' : 'ResizableCellLauncherData',
          MissileLoad: Array.isArray(currMount.componentData) && currMount.componentData.length > 0 ? {
            MagSaveData: currMount.componentData.map(e => {
              return {
                MagazineKey: this.randomKey(),
                MunitionKey: `$MODMIS$/${e.munitionName}`,
                Quantity: e.quantity
              };
            })
          } : {}
        };
      }
      if(currMount.componentName !== 'None') {
        ret.Fleet.Ships.Ship.SocketMap.HullSocket.push(currRet);
      }
    }
  }
  for(let i = 0;i < compartments.length;i++) {
    if(compartments[i] !== null) {
      let currCompartment = compartments[i];
      let currRet = {
        Key: dict.hullTypeSocketKeys[hullType].compartments[i],
        ComponentName: `Stock/${currCompartment.componentName}`
      };
      if(
        currCompartment.componentName === 'Bulk Magazine' ||
        currCompartment.componentName === 'Reinforced Magazine'
      ) {
        currRet.ComponentData = {
          '@_xsi:type': 'BulkMagazineData',
          Load: Array.isArray(currCompartment.componentData) && currCompartment.componentData.length > 0 ? {
            MagSaveData: currCompartment.componentData.map(e => {
              return {
                MagazineKey: this.randomKey(),
                MunitionKey: `Stock/${e.munitionName}`,
                Quantity: e.quantity
              };
            })
          } : {}
        };
      }
      if(currCompartment.componentName !== 'None') {
        ret.Fleet.Ships.Ship.SocketMap.HullSocket.push(currRet);
      }
    }
  }
  for(let i = 0;i < modules.length;i++) {
    if(modules[i] !== null) {
      let currModule = modules[i];
      let currRet = {
        Key: dict.hullTypeSocketKeys[hullType].modules[i],
        ComponentName: `Stock/${currModule.componentName}`
      };
      if(currModule.componentName !== 'None') {
        ret.Fleet.Ships.Ship.SocketMap.HullSocket.push(currRet);
      }
    }
  }
  for(let i = 0;i < missiles.length;i++) {
    if(missiles[i] !== null) {
      let currMissile = missiles[i];
      ret.Fleet.MissileTypes.MissileTemplate.push({
        Designation: currMissile.prefix,
        Nickname: currMissile.name,
        BodyKey: `Stock/${currMissile.body}`,
        TemplateKey: v4(),
        Sockets: {
          MissileSocket: currMissile.components.map(s => (
            typeof s.type !== 'string' && typeof s.name !== 'string' ?
              {
                Size: s.size
              }
            :
              {
                Size: s.size,
                InstalledComponent: typeof s.type === 'string' ? 
                  Object.assign({
                    '@_xsi:type': `${s.type}Settings`,
                    ComponentKey: typeof s.name === 'string' ? `Stock/${s.name}` : ''
                  }, s.settings)
                :
                  {
                    ComponentKey: `Stock/${s.name}`
                  }
              }
          ))
        }
      });
    }
  }
  return shipBuilder.build(ret).replace(/&apos;/g, '\'');
}