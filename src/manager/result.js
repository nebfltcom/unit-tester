const deepcopy = require('deepcopy');
const dict = require('../common/dict');

const convertComponentID = (hullType, componentId) => {
  let dictMounts = dict.hullTypeSocketKeys[hullType].mounts;
  for(let i = 0;i < dictMounts.length;i++) {
    if(dictMounts[i] === componentId) {
      return `Mount${i + 1}`;
    }
  }
  let dictCompartments = dict.hullTypeSocketKeys[hullType].compartments;
  for(let i = 0;i < dictCompartments.length;i++) {
    if(dictCompartments[i] === componentId) {
      return `Compartment${i + 1}`;
    }
  }
  let dictModules = dict.hullTypeSocketKeys[hullType].modules;
  for(let i = 0;i < dictModules.length;i++) {
    if(dictModules[i] === componentId) {
      return `Module${i + 1}`;
    }
  }
  return componentId;
}

exports.filterResult = (result) => {
  let res = deepcopy(result);
  res.eventLog.sort((e1, e2) => e1.Time - e2.Time);
  //Remove all events before ship data
  let foundShipData = false;
  while(!foundShipData) {
    if(res.eventLog[0].EventName !== 'ShipDataEvent') {
      res.eventLog.splice(0, 1);
    }
    else {
      foundShipData = true;
    }
  }
  let firstTime = res.eventLog[0].Time;
  for(let i = 0;i < res.eventLog.length;i++) {
    //Shorten timestamps to start from test start
    res.eventLog[i].Time -= firstTime;
    //Convert component ID
    let currEvent = res.eventLog[i];
    let hullType = currEvent.OriginShip === 'Attacker' ? res.test.setup.attackingShip.ship.hullType : res.test.setup.targetShip.ship.hullType;
    if(typeof currEvent.Components !== 'undefined') {
      for(let j = 0;j < currEvent.Components.length;j++) {
        res.eventLog[i].Components[j].ComponentID = convertComponentID(hullType, currEvent.Components[j].ComponentID);
      }
    }
    if(typeof currEvent.Component !== 'undefined') {
      res.eventLog[i].Component.ComponentID = convertComponentID(hullType, currEvent.Component.ComponentID);
    }
  }
  return res;
}